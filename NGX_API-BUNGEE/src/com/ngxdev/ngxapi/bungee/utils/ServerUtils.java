package com.ngxdev.ngxapi.bungee.utils;

import com.ngxdev.ngxapi.bungee.main.MainClass;

public class ServerUtils {
    public static String getServerByIP(String ip) {
        for (String s : MainClass.getMain().getProxy().getServers().keySet()) {
            if (MainClass.getMain().getProxy().getServerInfo(s).getAddress().getHostName().equals(ip)) {
                return s;
            }
        }
        return null;
    }

    public static String getServerByPort(int port) {
        for (String s : MainClass.getMain().getProxy().getServers().keySet()) {
            if (MainClass.getMain().getProxy().getServerInfo(s).getAddress().getPort() == port) {
                return s;
            }
        }
        return null;
    }

    public static int getServerSocketPort(String server) {
        return MainClass.getMain().cfg.getInt("socket.servers." + server);
    }
}
