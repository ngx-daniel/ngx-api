package com.ngxdev.ngxapi.bungee.API;

import com.ngxdev.ngxapi.bungee.main.MainClass;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Config {
    MainClass m;
    YamlConfiguration provider;
    private File file;
    private Configuration config;

    public Config(File configFile) {
        m = MainClass.getMain();
        provider = (YamlConfiguration) ConfigurationProvider.getProvider(YamlConfiguration.class);
        file = configFile;
        try {
            config = provider.load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object get(String path) {
        return config.get(path);
    }

    public Object get(String path, Object def) {
        return config.get(path, def);
    }

    public String getString(String path) {
        return config.getString(path);
    }

    public String getString(String path, String def) {
        return config.getString(path, def);
    }

    public int getInt(String path) {
        return config.getInt(path);
    }

    public int getInt(String path, int def) {
        return config.getInt(path, def);
    }

    public boolean getBoolean(String path) {
        return config.getBoolean(path);
    }

    public boolean getBoolean(String path, boolean def) {
        return config.getBoolean(path, def);
    }

    public double getDouble(String path) {
        return config.getDouble(path);
    }

    public double getDouble(String path, double def) {
        return config.getDouble(path, def);
    }

    public List<?> getList(String path) {
        return config.getList(path);
    }

    public List<String> getStringList(String path) {
        return config.getStringList(path);
    }

    public List<?> getList(String path, List<?> def) {
        return config.getList(path, def);
    }

    public void removeKey(String path) {
        config.set(path, null);
    }

    public boolean contains(String path) {
        return config.get(path) != null;
    }

    public void build(String path, Object value) {
        if (!contains(path)) {
            config.set(path, value);
            saveConfig();
        }
    }

    public void set(String path, Object value) {
        config.set(path, value);
        saveConfig();
    }

    public void saveConfig() {
        try {
            provider.save(config, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reloadConfig() {
        saveConfig();
        try {
            config = provider.load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unload() {
        saveConfig();
        provider = null;
        file = null;
        config = null;
    }
}
