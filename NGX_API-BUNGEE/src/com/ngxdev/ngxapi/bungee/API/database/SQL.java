package com.ngxdev.ngxapi.bungee.API.database;

import com.ngxdev.ngxapi.bungee.main.MainClass;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQL {
    private static SQL instance;

    private String host;
    private int port;
    private String user;
    private String password;
    private String database;
    private Connection con;

    public SQL() {
        MainClass m = MainClass.getMain();
        m.cfg.build("sql.host", "localhost");
        m.cfg.build("sql.port", 3306);
        m.cfg.build("sql.user", "ngxapi");
        m.cfg.build("sql.password", "password");
        m.cfg.build("sql.database", "ngxapi");
        host = m.cfg.getString("sql.host");
        port = m.cfg.getInt("sql.port");
        user = m.cfg.getString("sql.user");
        password = m.cfg.getString("sql.password");
        database = m.cfg.getString("sql.database");
    }

    public static void setup() {
        instance = new SQL();
    }

    public static SQL getInstance() {
        return SQL.instance;
    }

    public static List<String> getColumns(ResultSet rs) throws SQLException {
        List<String> list = new ArrayList<>();
        ResultSetMetaData rsMetaData = rs.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();

        for (int i = 1; i < numberOfColumns + 1; i++) {
            list.add(rsMetaData.getColumnName(i));
        }
        return list;
    }

    public String getDatabase() {
        return database;
    }

    public ResultSet sendQuery(String query) {
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (con == null) openConnection();
            st = con.prepareStatement(query);
            rs = st.executeQuery();
        } catch (SQLException e) {
            System.err.println("Failed to send update " + query + ".");
            e.printStackTrace();
            return null;
        } finally {
            closeResources(null, st);
        }
        closeResources(null, st);
        return rs;
    }

    public ResultSet sendUpdateQuery(String query) {
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (con == null) openConnection();
            st = con.prepareStatement(query);
            st.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Failed to send update " + query + ".");
            e.printStackTrace();
            return null;
        } finally {
            closeResources(null, st);
        }
        closeResources(null, st);
        return rs;
    }

    public void createTable(String table) {
        sendUpdateQuery("CREATE TABLE IF NOT EXISTS " + table + " (ID SERIAL PRIMARY KEY, info VARCHAR(100))");
    }

    public void createColumn(String table, String name) {
        try {
            if (con == null) openConnection();
            DatabaseMetaData md = con.getMetaData();
            ResultSet rs = md.getColumns(null, null, table, name);
            if (!rs.next()) {
                sendUpdateQuery("ALTER TABLE " + table + " ADD " + name + " VARCHAR(100)");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public boolean checkExist(String table, String info) {
        final String query = "SELECT EXISTS(SELECT 1 FROM " + table + " WHERE info = ?)";
        try (PreparedStatement st = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, info);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return rs.getBoolean(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void setData(String table, String source, String data, String value) {
        createColumn(table, data);
        sendUpdateQuery("UPDATE " + table + " SET " + data + "='" + value + "' WHERE info='" + source + "'");
    }

    public List<String> getRowData(String table) {
        List<String> list = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("SELECT * FROM " + table);
            rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("info"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return list;
        } finally {
            closeResources(rs, st);
        }
        closeResources(rs, st);
        return list;
    }

    public String getData(String table, String source, String data) {
        ResultSet rs = null;
        PreparedStatement st = null;
        String sfield = null;
        try {
            st = con.prepareStatement("SELECT * FROM " + table + " WHERE info=?", ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, source);
            rs = st.executeQuery();
            rs.last();
            if (rs.getRow() != 0) {
                rs.first();
                sfield = rs.getString(data);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            closeResources(rs, st);
        }
        closeResources(rs, st);
        return sfield;
    }

    public void closeResources(final ResultSet rs, final PreparedStatement st) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection openConnection() {
        if (con == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                String connectionscript = "jdbc:mysql://" + host + ":" + port + "/" + database;
                con = DriverManager.getConnection(connectionscript, user, password);
                return con;
            } catch (Exception e) {
                e.printStackTrace();
                MainClass.getMain().getProxy().getLogger().severe("SQL CONNECTION FAILED");
                return null;
            }
        } else {
            return con;
        }
    }
}
