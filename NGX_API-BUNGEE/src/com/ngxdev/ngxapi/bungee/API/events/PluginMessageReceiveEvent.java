package com.ngxdev.ngxapi.bungee.API.events;

import net.md_5.bungee.api.plugin.Event;

public class PluginMessageReceiveEvent extends Event {
    private String channel,
            message;

    public PluginMessageReceiveEvent(String channel, String data) {
        this.channel = channel;
        this.message = data;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }
}
