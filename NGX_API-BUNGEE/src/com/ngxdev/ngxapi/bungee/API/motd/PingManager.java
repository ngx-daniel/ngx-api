package com.ngxdev.ngxapi.bungee.API.motd;

public class PingManager {

    private static Class<? extends ServerStatus> pingExecutor;
    private static int stopAfter = 100;

    public static Class<? extends ServerStatus> getPingManager() {
        return pingExecutor;
    }

    public static void setPingManager(Class<? extends ServerStatus> pingManager) {
        pingExecutor = pingManager;
    }

    public static int getStopAfter() {
        return stopAfter;
    }

    public static void setStopAfter(int seconds) {
        PingManager.stopAfter = seconds;
    }
}
