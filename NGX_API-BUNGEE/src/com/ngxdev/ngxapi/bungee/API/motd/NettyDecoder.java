package com.ngxdev.ngxapi.bungee.API.motd;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ngxdev.ngxapi.bungee.main.MainClass;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.protocol.PacketWrapper;
import net.md_5.bungee.protocol.packet.Handshake;
import net.md_5.bungee.protocol.packet.PingPacket;
import net.md_5.bungee.protocol.packet.StatusRequest;
import net.md_5.bungee.protocol.packet.StatusResponse;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class NettyDecoder extends MessageToMessageDecoder<PacketWrapper> {

    private final MainClass plugin;
    private final long startTime;
    private ServerStatus statusListener;
    private long previousTime = System.currentTimeMillis();
    private ChannelHandlerContext ctx;
    private ScheduledTask task;
    private boolean isPing = false;
    private long requestedProtocol = -1;
    private boolean respondPing = false;

    public NettyDecoder(MainClass plugin) {
        this.plugin = plugin;
        this.startTime = System.currentTimeMillis();
        try {
            this.statusListener = PingManager.getPingManager().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        task = ProxyServer.getInstance().getScheduler().schedule(this.plugin, new Runnable() {

            public void run() {
                if (!isPing) {
                    task.cancel();
                }
                if ((System.currentTimeMillis() - previousTime) > 5000) {
                    if (isPing) {
                        ctx.close();
                        System.out.println("Animated Ping handler disconnected.");
                    }
                    task.cancel();
                }
            }
        }, 15L, 15L, TimeUnit.SECONDS);
    }

    @Override
    protected void decode(final ChannelHandlerContext ctx, PacketWrapper packet, List<Object> out) throws Exception {
        this.previousTime = System.currentTimeMillis();
        this.ctx = ctx;

        if (packet == null || packet.packet == null) {
            out.add(packet);
            return;
        }
        if (packet.packet instanceof Handshake) {
            Handshake packett = (Handshake) packet.packet;
            this.requestedProtocol = packett.getProtocolVersion();
        }
        if (packet.packet instanceof PingPacket) {
            if (respondPing) {
                ctx.pipeline().writeAndFlush(new PingPacket(((PingPacket) packet.packet).getTime()));
                ctx.close();
                return;
            }
            if ((System.currentTimeMillis() - startTime) > (PingManager.getStopAfter() * 1000)) {
                final ServerData data = this.statusListener.update();

                JsonObject version = new JsonObject();
                version.addProperty("name", "1.8");
                version.addProperty("protocol", requestedProtocol);

                JsonArray playerArray = new JsonArray();
                for (String playerName : data.getPlayers()) {
                    JsonObject playerObject = new JsonObject();
                    playerObject.addProperty("name", playerName);
                    playerObject.addProperty("id", UUID.randomUUID().toString());
                    playerArray.add(playerObject);
                }

                JsonObject countData = new JsonObject();
                countData.addProperty("max", getMaxCount());
                countData.addProperty("online", ProxyServer.getInstance().getOnlineCount());
                countData.add("sample", playerArray);

                JsonObject jsonObject = new JsonObject();
                jsonObject.add("version", version);
                jsonObject.add("players", countData);
                jsonObject.addProperty("description", data.getMotd());

                if (data.getFavicon() != null) {
                    jsonObject.addProperty("favicon", data.getFavicon());
                }
                ctx.pipeline().writeAndFlush(new StatusResponse(jsonObject.toString()));
                respondPing = true;
            } else {
                final ServerData data = this.statusListener.update();
                this.isPing = true;
                ProxyServer.getInstance().getScheduler().schedule(this.plugin, new Runnable() {

                    public void run() {
                        ctx.pipeline().writeAndFlush((new StatusResponse(buildResponseJSON(data))));
                    }
                }, data.getSleepMillis(), TimeUnit.MILLISECONDS);
            }
        } else if (packet.packet instanceof StatusRequest) {
            final ServerData data = this.statusListener.update();
            ctx.pipeline().writeAndFlush(new StatusResponse(buildResponseJSON(data)));
        } else {
            out.add(packet);
        }
    }

    @SuppressWarnings("deprecation")
    private int getMaxCount() {
        for (ListenerInfo listener : ProxyServer.getInstance().getConfig().getListeners()) {
            return listener.getMaxPlayers();
        }
        return -1;
    }

    private String buildResponseJSON(ServerData d) {
        final ServerData data = d;

        JsonObject version = new JsonObject();
        version.addProperty("name", data.getFormat().replace("%COUNT%", ProxyServer.getInstance().getOnlineCount() + "").replace("%MAX%", getMaxCount() + ""));
        version.addProperty("protocol", 10000);

        JsonArray playerArray = new JsonArray();
        for (String playerName : data.getPlayers()) {
            JsonObject playerObject = new JsonObject();
            playerObject.addProperty("name", playerName);
            playerObject.addProperty("id", UUID.randomUUID().toString());
            playerArray.add(playerObject);
        }

        JsonObject countData = new JsonObject();
        countData.addProperty("max", 100000);
        countData.addProperty("online", MainClass.getMain().getProxy().getOnlineCount());
        countData.add("sample", playerArray);

        JsonObject jsonObject = new JsonObject();
        jsonObject.add("version", version);
        jsonObject.add("players", countData);
        jsonObject.addProperty("description", data.getMotd());

        if (data.getFavicon() != null) {
            jsonObject.addProperty("favicon", data.getFavicon());
        }
        return jsonObject.toString();
    }
}
