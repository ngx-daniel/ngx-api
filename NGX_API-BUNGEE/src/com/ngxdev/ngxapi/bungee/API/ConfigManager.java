package com.ngxdev.ngxapi.bungee.API;

import net.md_5.bungee.api.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class ConfigManager {
    public Config getNewConfig(String filePath, Plugin plugin) {
        File file = this.getConfigFile(filePath, plugin);

        if (!file.exists()) {
            this.prepareFile(filePath, plugin);
        }

        Config config = new Config(file);
        return config;

    }

    private File getConfigFile(String file, Plugin plugin) {
        if (file.isEmpty())
            return null;
        File configFile;
        if (file.contains("/"))
            if (file.startsWith("/"))
                configFile = new File(plugin.getDataFolder() + file.replace("/", File.separator));
            else
                configFile = new File(plugin.getDataFolder() + File.separator + file.replace("/", File.separator));
        else
            configFile = new File(plugin.getDataFolder(), file);
        return configFile;
    }

    public void prepareFile(String filePath, Plugin plugin) {
        File file = this.getConfigFile(filePath, plugin);
        if (file.exists())
            return;
        try {
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}