package com.ngxdev.ngxapi.bungee.API;

import com.ngxdev.ngxapi.bungee.API.events.PluginMessageReceiveEvent;
import com.ngxdev.ngxapi.bungee.API.socket.SocketDecoder;
import com.ngxdev.ngxapi.bungee.main.MainClass;
import com.ngxdev.ngxapi.bungee.utils.ServerUtils;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ServerPing;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketAPI {
    private static ServerSocket socket;
    private static MainClass m;
    private static int port;
    private static String password;

    public static void close() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void decode(String data) {
        m.getProxy().getLogger().info("RECEIVED MESSAGE: " + data);
        String[] args = data.split("\\|");
        String datapassword = args[0];
        String dataserver = args[1];
        final String datachannel = args[2];
        final String datamessage = args[3];
        if (!password.equals(datapassword)) {
            m.getProxy().getLogger().info("[SECURITY] Send data attempt from unauthorized socket. Wrong password: " + datapassword);
            return;
        }
        if (dataserver.equalsIgnoreCase("bungee")) {
            PluginMessageReceiveEvent event = new PluginMessageReceiveEvent(datachannel, datamessage);
            m.getProxy().getPluginManager().callEvent(event);
            return;
        } else if (dataserver.equalsIgnoreCase("all")) {
            for (final String server : m.getProxy().getServers().keySet()) {
                m.getProxy().getServers().get(server).ping(new Callback<ServerPing>() {
                    @Override
                    public void done(ServerPing serverPing, Throwable error) {
                        if (error == null) sendData(server, datamessage, datachannel);
                        else
                            m.getProxy().getLogger().info("Failed to send socket message to " + server + " because it's offline.");
                    }
                });
            }
            return;
        } else if (m.getProxy().getServerInfo(dataserver) == null) {
            m.getProxy().getLogger().info("[ERROR] Unknown dataserver, something went wrong on the developers side. Unknown server: " + dataserver);
            return;
        }
        if (ServerUtils.getServerSocketPort(dataserver) == 0) {
            m.getProxy().getLogger().info("[ERROR] No socket port was set for this server. ServerAPI setPort() failed");
            return;
        }
        sendData(dataserver, datamessage, datachannel);
    }

    public static void sendData(String server, String data, String channel) {
        try {
            Socket client = new Socket(m.getProxy().getServerInfo(server).getAddress().getHostName(), ServerUtils.getServerSocketPort(server));
            DataOutputStream ds = new DataOutputStream(client.getOutputStream());
            ds.writeUTF(compile(channel, data));
            ds.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String compile(String channel, String data) {
        return channel + "|" + data;
    }

    public void setup() {
        m = MainClass.getMain();
        password = m.cfg.getString("socket.password");
        port = m.cfg.getInt("socket.port");
        new SocketDecoder();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new ServerSocket(port);
                    while (true) {
                        Socket s = socket.accept();
                        decode(new DataInputStream(s.getInputStream()).readUTF());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
