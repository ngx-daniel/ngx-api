package com.ngxdev.ngxapi.bungee.API.socket;

import com.ngxdev.ngxapi.bungee.API.events.PluginMessageReceiveEvent;
import com.ngxdev.ngxapi.bungee.main.InfoUpdater;
import com.ngxdev.ngxapi.bungee.main.MainClass;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class SocketDecoder implements Listener {
    public SocketDecoder() {
        MainClass.getMain().getProxy().getPluginManager().registerListener(MainClass.getMain(), this);
    }

    @EventHandler
    public void decode(PluginMessageReceiveEvent e) {
        switch (e.getChannel().toLowerCase()) {
            case "update":
                switch (e.getMessage().toLowerCase()) {
                    case "servers":
                        InfoUpdater.run();
                        break;
                }
                break;
        }
    }
}
