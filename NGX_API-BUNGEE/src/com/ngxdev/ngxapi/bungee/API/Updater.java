package com.ngxdev.ngxapi.bungee.API;

import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Updater {
    private String url;
    private String destination;

    public Updater(String url, String destination) {
        this.url = url;
        this.destination = destination;
    }

    public void start() {
        try {
            URL website = new URL(this.url);
            ReadableByteChannel e = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(this.destination);
            fos.getChannel().transferFrom(e, 0L, 9223372036854775807L);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

