package com.ngxdev.ngxapi.bungee.main;

import com.ngxdev.ngxapi.bungee.API.database.SQL;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ServerPing;

public class InfoUpdater {
    public static int task;

    public InfoUpdater() {
        MainClass m = MainClass.getMain();
        final SQL database = SQL.getInstance();
        database.openConnection();
        database.createTable("servers");
        for (final String server : m.getProxy().getServers().keySet()) {
            if (!database.checkExist("servers", server)) {
                database.sendUpdateQuery("INSERT INTO servers (info) VALUES ('" + server + "')");
            }
        }
        if (!database.checkExist("servers", "all")) {
            database.sendUpdateQuery("INSERT INTO servers (info) VALUES ('all')");
        }
    }

    public static void run() {
        MainClass m = MainClass.getMain();
        int total = m.getProxy().getOnlineCount();
        final SQL database = SQL.getInstance();
        database.openConnection();
        database.setData("servers", "all", "online", Integer.toString(total));
        for (final String server : m.getProxy().getServers().keySet()) {
            m.getProxy().getServerInfo(server).ping(new Callback<ServerPing>() {
                @Override
                public void done(ServerPing ping, Throwable error) {
                    if (!database.checkExist("servers", server)) {
                        database.sendUpdateQuery("INSERT INTO servers (info) VALUES ('" + server + "')");
                    }
                    if (error != null) {
                        database.setData("servers", server, "online", "-1");
                        database.setData("servers", server, "status", "false");
                    } else {
                        database.setData("servers", server, "online", Integer.toString(ping.getPlayers().getOnline()));
                        database.setData("servers", server, "status", "true");
                    }
                }
            });
        }
    }
}
