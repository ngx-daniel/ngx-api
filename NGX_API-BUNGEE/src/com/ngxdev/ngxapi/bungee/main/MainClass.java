package com.ngxdev.ngxapi.bungee.main;

import com.ngxdev.ngxapi.bungee.API.Config;
import com.ngxdev.ngxapi.bungee.API.ConfigManager;
import com.ngxdev.ngxapi.bungee.API.SocketAPI;
import com.ngxdev.ngxapi.bungee.API.Updater;
import com.ngxdev.ngxapi.bungee.API.database.SQL;
import com.ngxdev.ngxapi.bungee.API.events.PluginMessageReceiveEvent;
import com.ngxdev.ngxapi.bungee.API.motd.ConnectionReplacement;
import com.ngxdev.ngxapi.bungee.utils.ServerUtils;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.netty.PipelineUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class MainClass extends Plugin implements Listener {
    private static MainClass m;
    public Config cfg;

    public static MainClass getMain() {
        return m;
    }

    @Override
    public void onLoad() {
        m = this;
    }

    @Override
    public void onEnable() {
        setStaticFinalValue();
        buildConfig();
        new SocketAPI().setup();
        SQL.setup();
        new InfoUpdater();
        m.getProxy().getPluginManager().registerListener(m, m);
    }

    @Override
    public void onDisable() {
        m.getProxy().getScheduler().cancel(InfoUpdater.task);
        new Updater("https://www.dropbox.com/s/annkjb6lj9lo6gf/NGX_API-BUNGEE.jar?dl=1", "plugins/NGX_API-BUNGEE.jar").start();
        SocketAPI.close();
    }

    private void buildConfig() {
        cfg = new ConfigManager().getNewConfig("config.yml", this);
        cfg.build("socket.port", 1337);
        cfg.build("socket.password", "password");
        for (String s : getProxy().getServers().keySet()) {
            cfg.build("socket.servers." + s, 0);
        }
    }

    private void setStaticFinalValue() {
        try {
            Field field = PipelineUtils.class.getDeclaredField("SERVER_CHILD");
            field.setAccessible(true);
            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
            field.set(null, new ConnectionReplacement(this));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPluginMessageReveive(PluginMessageReceiveEvent e) {
        if (e.getChannel().equalsIgnoreCase("setport")) {
            String args[] = e.getMessage().split(":");
            String server = ServerUtils.getServerByPort(Integer.parseInt(args[0]));
            int port = Integer.parseInt(args[1]);
            m.getProxy().getLogger().info(server + "    " + port);
            m.cfg.set("socket.servers." + server, port);
        }
    }
}
