package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.API.DownloadManager;
import com.ngxdev.ngxapi.server.API.ZipManager;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.main.NGXserver;
import com.ngxdev.ngxapi.server.utils.NGXfileUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

public class NGXmap {
    private static ArrayList<NGXmap> maps = new ArrayList<>();
    private static ArrayList<World> worlds = new ArrayList<>();
    private ArrayList<Location> spec = new ArrayList<>(),
            orange = new ArrayList<>(),
            magenta = new ArrayList<>(),
            light_blue = new ArrayList<>(),
            yellow = new ArrayList<>(),
            lime = new ArrayList<>(),
            pink = new ArrayList<>(),
            gray = new ArrayList<>(),
            silver = new ArrayList<>(),
            cyan = new ArrayList<>(),
            purple = new ArrayList<>(),
            blue = new ArrayList<>(),
            brown = new ArrayList<>(),
            green = new ArrayList<>(),
            red = new ArrayList<>(),
            black = new ArrayList<>();
    private MainClass m;
    private String name;
    private URL url;
    private World w;
    private String worldname;
    private boolean downloaddone,
            downloading;

    public NGXmap(String name, String url) {
        m = MainClass.getMain();
        maps.add(this);
        this.name = name;
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void destroyWorlds() {
        MainClass m = MainClass.getMain();
        for (World w : worlds) {
            for (Player p : w.getPlayers()) {
                p.teleport(new Location(m.getServer().getWorld(m.gameconfig.getString("spawn.world")),
                        m.gameconfig.getDouble("spawn.x"),
                        m.gameconfig.getDouble("spawn.y"),
                        m.gameconfig.getDouble("spawn.z")));
            }
            File world = w.getWorldFolder().getAbsoluteFile();
            m.getServer().unloadWorld(w, true);
            NGXfileUtils.deleteDirectory(world);
        }
        worlds.clear();
    }

    public World getWorld() {
        return w;
    }

    public ArrayList<Location> getSpecSpawns() {
        return spec;
    }

    public ArrayList<Location> getTeamSpawns(NGXteam team) {
        if (team == NGXteam.BLACK) {
            return black;
        } else if (team == NGXteam.RED) {
            return red;
        } else if (team == NGXteam.GREEN) {
            return green;
        } else if (team == NGXteam.BROWN) {
            return brown;
        } else if (team == NGXteam.BLUE) {
            return blue;
        } else if (team == NGXteam.PURPLE) {
            return purple;
        } else if (team == NGXteam.CYAN) {
            return cyan;
        } else if (team == NGXteam.SILVER) {
            return silver;
        } else if (team == NGXteam.GRAY) {
            return gray;
        } else if (team == NGXteam.PINK) {
            return pink;
        } else if (team == NGXteam.LIME) {
            return lime;
        } else if (team == NGXteam.YELLOW) {
            return yellow;
        } else if (team == NGXteam.LIGHT_BLUE) {
            return light_blue;
        } else if (team == NGXteam.MAGENTA) {
            return magenta;
        } else if (team == NGXteam.ORANGE) {
            return orange;
        } else {
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public void preloadWorld() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                downloaddone = false;
                worldname = prepareWorld();
                downloaddone = true;
            }
        }).start();
    }

    public boolean isDownloaded() {
        return downloaddone;
    }

    public boolean isDownloading() {
        return downloading;
    }

    public void loadWorld() {
        WorldCreator dwc = new WorldCreator(worldname);
        dwc.environment(World.Environment.NORMAL);
        dwc.generator(new EmptyWorld());
        w = m.getServer().createWorld(dwc);
        worlds.add(w);
        int radius = 100;
        final Block spawn = w.getBlockAt(w.getSpawnLocation());
        final int minY = 0;
        final int maxY = 255;
        for (int X = spawn.getX() - radius; X <= spawn.getX() + radius; X++) {
            for (int Y = minY; Y <= maxY; Y++) {
                for (int Z = spawn.getZ() - radius; Z <= spawn.getZ() + radius; Z++) {
                    final Block cb = w.getBlockAt(X, Y, Z);
                    if (!(cb.getType() == Material.SIGN_POST || cb.getType() == Material.WALL_SIGN)) continue;
                    Location from = cb.getLocation();
                    Location to = spawn.getLocation();
                    from.add(0.5, 0, 0.5);
                    from.setPitch(0);
                    Sign sign = (Sign) cb.getState();
                    if (sign.getLine(0) == null) continue;
                    if (sign.getLine(1) == null) continue;
                    from.setYaw(sign.getLocation().getYaw());
                    if (sign.getLine(0).equals("%spawn%")) {
                        if (sign.getLine(1).equalsIgnoreCase("BLACK")) black.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("RED")) red.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("GREEN")) green.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("RED")) red.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("BLUE")) blue.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("PURPLE")) purple.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("CYAN")) cyan.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("GRAY")) gray.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("SILVER")) silver.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("PINK")) pink.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("LIME")) lime.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("ORANGE")) orange.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("BLUE")) blue.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("CYAN")) cyan.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("MAGENTA")) magenta.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("YELLOW")) yellow.add(from);
                        else if (sign.getLine(1).equalsIgnoreCase("SPEC")) spec.add(from);
                        cb.setType(Material.AIR);
                    }
                }
            }
        }
        NGXserver.get().debug("SPAWNS SET");
    }

    public void destroy() {
        for (Player p : w.getPlayers()) {
            p.teleport(new Location(m.getServer().getWorld(m.gameconfig.getString("spawn.world")),
                    m.gameconfig.getDouble("spawn.x"),
                    m.gameconfig.getDouble("spawn.y"),
                    m.gameconfig.getDouble("spawn.z")));
        }
        File world = w.getWorldFolder().getAbsoluteFile();
        m.getServer().unloadWorld(w, true);
        NGXfileUtils.deleteDirectory(world);
        worlds.remove(w);
        downloading = false;
        downloaddone = false;
        w = null;
    }

    private String prepareWorld() {
        downloading = true;
        File tempdir = new File("temp");
        if (!tempdir.exists()) tempdir.mkdir();
        String downloaded = UUID.randomUUID() + "";
        new DownloadManager(url, "temp/" + downloaded + ".zip").start();
        new ZipManager("temp/" + downloaded + ".zip", downloaded).unzip();
        NGXserver.get().info(new File("temp/" + downloaded + ".zip") + "");
        new File("temp/" + downloaded + ".zip").delete();
        downloading = false;
        return downloaded;
    }

    class EmptyWorld extends ChunkGenerator {
        public List<BlockPopulator> getDefaultPopulators(World world) {
            return Arrays.asList(new BlockPopulator[0]);
        }

        public boolean canSpawn(World world, int x, int z) {
            return true;
        }

        public byte[] generate(World world, Random rand, int chunkx, int chunkz) {
            return new byte[32768];
        }

        public Location getFixedSpawnLocation(World world, Random random) {
            return new Location(world, 0, 128, 0);
        }

    }
}
