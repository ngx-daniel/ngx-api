package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.events.ServerPredisableEvent;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class NGXgameListeners implements Listener {
    public NGXgameListeners() {
        ListenerManager.register(this);
    }

    @EventHandler
    public void DamageEvent(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player p = (Player) e.getEntity();
        NGXplayer np = NGXplayer.getNGXplayer(p);
        NGXgame pgame = NGXgame.getPlayerGame(np);
        if (pgame == null) return;
        if (pgame.isInvincibility()) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void InteractEvent(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        NGXplayer np = NGXplayer.getNGXplayer(p);
        NGXgame pgame = NGXgame.getPlayerGame(np);
        if (pgame == null) return;
        if (pgame.isInvincibility()) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void death(EntityDamageEvent e) {
        Entity en = e.getEntity();
        if (!(en instanceof Player)) return;
        Player p = (Player) en;
        NGXplayer np = NGXplayer.getNGXplayer(p);
        NGXgame pgame = NGXgame.getPlayerGame(np);
        if (pgame == null) return;
        if ((p.getHealth() - e.getDamage()) <= 0) {
            e.setCancelled(true);
            p.setHealth(20);
            p.setFoodLevel(20);
            pgame.setSpectator(np);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void death(PlayerDeathEvent e) {
        Player p = e.getEntity();
        NGXplayer np = NGXplayer.getNGXplayer(p);
        NGXgame pgame = NGXgame.getPlayerGame(np);
        if (pgame == null) return;
        p.teleport(p.getLocation());
        p.setHealth(20);
        e.setDeathMessage(null);
        pgame.setSpectator(np);
    }

    @EventHandler
    public void quit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        NGXplayer np = NGXplayer.getNGXplayer(p);
        NGXgame pgame = NGXgame.getPlayerGame(np);
        if (pgame == null) return;
        pgame.clearPlayer(np);
    }

    @EventHandler
    public void preDisable(ServerPredisableEvent e) {
        NGXmap.destroyWorlds();
    }
}
