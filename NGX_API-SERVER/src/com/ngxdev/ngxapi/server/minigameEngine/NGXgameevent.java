package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import org.bukkit.event.Listener;

public abstract class NGXgameevent implements Listener {
    public NGXgame game;
    MainClass m;

    public NGXgameevent() {
        m = MainClass.getMain();
    }

    public void register() {
        ListenerManager.register(this);
    }

    public void unregister() {
        ListenerManager.unregister(this);
    }

    public boolean check(NGXplayer np) {
        NGXgame pgame = NGXgame.getPlayerGame(np);
        return pgame != null && pgame.getName().equalsIgnoreCase(game.getName());
    }
}
