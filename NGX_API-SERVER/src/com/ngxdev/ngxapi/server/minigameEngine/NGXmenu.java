package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.events.GameJoinEvent;
import com.ngxdev.ngxapi.server.main.NGXserver;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

public class NGXmenu implements Listener {

    public NGXmenu() {
        ListenerManager.register(this);
    }

    public static void openGameMenu(NGXplayer p) {
        Inventory i = NGXserver.get().createInventory(null, 9, CommonUtils.convert("&6&lSelect a game"));
        for (NGXgame game : NGXgame.getGames()) {
            ItemStack item = new ItemStack(game.getMaterial());
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(CommonUtils.convert(game.getPrefix()));
            item.setItemMeta(meta);
            i.addItem(item);
        }
        p.getPlayer().openInventory(i);
    }

    public static void openTeamMenu(NGXplayer p, NGXgame game) {
        Inventory i = NGXserver.get().createInventory(null, 9, CommonUtils.convert("&6&lSelect a team"));
        for (NGXteam team : game.getTeams()) {
            ItemStack item = new ItemStack(Material.WOOL, 1, team.getData());
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(CommonUtils.convert(team.getName()));
            item.setItemMeta(meta);
            i.addItem(item);
        }
        p.getPlayer().openInventory(i);
    }

    public static void openKitMenu(NGXplayer p, NGXgame game) {
        Inventory i = NGXserver.get().createInventory(null, 9, CommonUtils.convert("&6&lSelect a kit"));
        for (NGXkit kit : game.getKits()) {
            ItemStack item = new ItemStack(kit.getMaterial());
            ItemMeta meta = item.getItemMeta();
            List<String> list = new ArrayList<>();
            meta.setDisplayName(kit.getName());
            if (kit.getMainAbility() != null) {
                list.add("");
                list.add("&e&lAbilitys:");
                list.add("&a&l" + kit.getMainAbility().getName() + " &f- " + kit.getMainAbility().getDesc());
            }
            if (kit.getSecondaryAbility() != null)
                list.add("&a&l" + kit.getSecondaryAbility().getName() + " &f- " + kit.getSecondaryAbility().getDesc());
            if (kit.getPotionEffects() != null) {
                list.add("");
                list.add("&e&lPassive Effects:");
                for (PotionEffect potion : kit.getPotionEffects()) {
                    list.add("&a" + potion.getType().getName());
                }
            }
            list.add("");
            if (kit.getHelmet() != null) list.add("&e&lHelmet: &a" + kit.getHelmet().getType());
            if (kit.getChestplate() != null) list.add("&e&lHelmet: &a" + kit.getChestplate().getType());
            if (kit.getLeggings() != null) list.add("&e&lHelmet: &a" + kit.getLeggings().getType());
            if (kit.getBoots() != null) list.add("&e&lHelmet: &a" + kit.getBoots().getType());
            meta.setLore(CommonUtils.convert(list));
            item.setItemMeta(meta);
            i.addItem(item);
        }
        p.getPlayer().openInventory(i);
    }

    @EventHandler(ignoreCancelled = true)
    public void GameMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        NGXplayer np = NGXplayer.getNGXplayer(p);
        ItemStack item = e.getCurrentItem();

        if (!e.getInventory().getName().equals(CommonUtils.convert("&6&lSelect a game"))) return;
        e.setCancelled(true);
        if (NGXgame.getPlayerGame(np) != null) {
            np.sendMessage("&4Error&f: &cYou are already in a game!");
            return;
        }

        NGXgame game = NGXgame.matchGameByPrefix(item.getItemMeta().getDisplayName());
        GameJoinEvent event = new GameJoinEvent(p, game);
        Bukkit.getServer().getPluginManager().callEvent(event);
        NGXenum response = game.addPlayer(np);
        if (response == NGXenum.QUEUEL)
            np.sendMessage("&cYou have been added to the game queuel, because the game was full.");
        else if (response == NGXenum.CURRENT) np.sendMessage("&a&lYou have joined the game.");
        p.closeInventory();
    }

    @EventHandler(ignoreCancelled = true)
    public void TeamMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        NGXplayer np = NGXplayer.getNGXplayer(p);
        ItemStack item = e.getCurrentItem();

        if (!e.getInventory().getName().equals(CommonUtils.convert("&6&lSelect a team"))) {
            return;
        }
        e.setCancelled(true);

        NGXgame game = NGXgame.getPlayerGame(np);
        if (game == null) {
            np.sendMessage("&4Error&f: &cYour not in a game!");
            return;
        }

        game.setTeam(np, NGXteam.matchTeam(item.getItemMeta().getDisplayName()));
    }

    @EventHandler(ignoreCancelled = true)
    public void KitMenu(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        NGXplayer np = NGXplayer.getNGXplayer(p);
        ItemStack item = e.getCurrentItem();

        if (!e.getInventory().getName().equals(CommonUtils.convert("&6&lSelect a kit"))) {
            return;
        }
        e.setCancelled(true);

        NGXgame game = NGXgame.getPlayerGame(np);
        if (game == null) {
            np.sendMessage("&4Error&f: &cYour not in a game!");
            return;
        }

        game.setKit(np, NGXkit.matchKit(item.getItemMeta().getDisplayName()));
        p.closeInventory();
    }
}
