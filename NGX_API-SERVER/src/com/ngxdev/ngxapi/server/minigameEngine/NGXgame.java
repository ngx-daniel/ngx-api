package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.interfaces.NGXgametick;
import com.ngxdev.ngxapi.server.interfaces.NGXminigame;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.main.NGXserver;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.*;

public abstract class NGXgame implements NGXminigame, NGXgametick, Listener {
    private static ArrayList<NGXgame> games = new ArrayList<>();
    private static HashMap<NGXplayer, NGXgame> gplayers = new HashMap<>();
    public MainClass m;
    public NGXserver server;
    public String name,
            description,
            status = "",
            prefix;
    public NGXplayer first,
            second,
            third;
    private NGXgame game;
    private int defaultcooldown,
            playersmin,
            playersmax,
            invincibilitytimer,
            locationtp,
            countdown;
    private boolean isRunning = false,
            invincibility = false;
    private NGXmap map;
    private Material mat;
    private ArrayList<NGXkit> kits = new ArrayList<>();
    private ArrayList<NGXteam> teams = new ArrayList<>();
    private ArrayList<NGXmap> maps = new ArrayList<>();
    private ArrayList<NGXgameevent> gameevents = new ArrayList<>();
    private ArrayList<NGXplayer> ingame = new ArrayList<>(),
            playing = new ArrayList<>(),
            players = new ArrayList<>(),
            queuel = new ArrayList<>();
    private HashMap<NGXplayer, NGXkit> pkit = new HashMap<>();
    private HashMap<NGXplayer, NGXteam> pteam = new HashMap<>();

    public NGXgame(String name, String prefix, String description, int playersmin, int playersmax, Material mat) {
        m = MainClass.getMain();
        server = NGXserver.get();
        games.add(this);
        game = this;
        this.name = name;
        this.playersmin = playersmin;
        this.playersmax = playersmax;
        this.mat = mat;
        this.prefix = prefix;
        this.description = description;
        setDefaultCooldown(m.gameconfig.getInt("countdown"));
        startGametask();
    }

    public static void setup() {
        MainClass m = MainClass.getMain();
        m.gameconfig.build("countdown", 30);
        NGXgameticker.startTicker(m);
        new NGXgameListeners();
        new NGXmenu();
    }

    public static NGXgame matchGame(String s) {
        for (NGXgame game : games) {
            if (game.getName().equalsIgnoreCase(s)) {
                return game;
            }
        }
        return null;
    }

    public static NGXgame matchGameByPrefix(String s) {
        for (NGXgame game : games) {
            if (CommonUtils.convert(game.getPrefix()).equalsIgnoreCase(s)) {
                return game;
            }
        }
        return null;
    }

    public static ArrayList<NGXgame> getGames() {
        return games;
    }

    public static NGXgame getPlayerGame(NGXplayer p) {
        if (gplayers.get(p) == null) {
            return null;
        } else {
            return gplayers.get(p);
        }
    }

    public void clearPlayer(NGXplayer p) {
        players.remove(p);
        gplayers.remove(p);
        playing.remove(p);
        ingame.remove(p);
        queuel.remove(p);
        pteam.remove(p);
        pkit.remove(p);
        p.getPlayer().teleport(new Location(m.getServer().getWorld(m.gameconfig.getString("spawn.world")),
                m.gameconfig.getDouble("spawn.x"),
                m.gameconfig.getDouble("spawn.y"),
                m.gameconfig.getDouble("spawn.z")));
    }

    public Material getMaterial() {
        return mat;
    }

    public void setTeam(NGXplayer p, NGXteam t) {
        if (!(pteam.containsKey(p) && pteam.get(p) == t)) pteam.put(p, t);
    }

    public NGXenum setKit(NGXplayer p, NGXkit k) {
        if (pkit.containsKey(p) && pkit.get(p) == k) {
            return NGXenum.ALREADY_SELECTED;
        } else {
            pkit.put(p, k);
            return NGXenum.KIT_SELECTED;
        }
    }

    public void startGametask() {
        final NGXserver server = NGXserver.get();
        server.getScheduler().scheduleSyncRepeatingTask(m, new Runnable() {
            @Override
            public void run() {
                if (map != null) {
                    if (!map.isDownloaded() && map.isDownloading()) {
                        status = "Downloading map.";
                        return;
                    } /*else {
                        if (playersmax == 0) {
                            int max = 0;
                            for (NGXteam t : NGXteam.values()) {
                                List<Location> locations = map.getTeamSpawns(t);
                                if (locations != null) {
                                    max += locations.size();
                                }
                            }
                            playersmax = max;
                        }
                    }*/
                } else {
                    prepareMap();
                }
                if (isRunning) {
                    status = "Running";
                    setCooldown(defaultcooldown);
                    if (invincibilitytimer >= 0) {
                        for (NGXplayer np : ingame) {
                            np.packet.sendActionBar("&f&lStarting: &6&l[" + CommonUtils.toBar("|||||", invincibilitytimer, 10) + "&6&l]");
                        }
                        if (invincibilitytimer <= 5) {
                            for (NGXplayer p : ingame) {
                                p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.ORB_PICKUP, 100, 100);
                            }

                        }
                        invincibilitytimer--;
                    } else {
                        for (NGXplayer np : ingame) {
                            np.packet.sendActionBar("");
                        }
                        if (invincibility) {
                            invincibility = false;
                            invincibilitytimer = -1;
                            setupGameevents();
                            NGXgameticker.registerTicker(game);
                            ListenerManager.register(game);
                        }
                    }
                } else {
                    if (players.size() >= playersmin) {
                        status = "Countdown&f: " + countdown;
                        if (countdown > 0) {
                            if (countdown <= 5) {
                                for (NGXplayer p : players) {
                                    p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.ORB_PICKUP, 100, 100);
                                    p.sendMessage("&a&lStarting game in &6" + countdown + " &asecond" + (countdown <= 1 ? "" : "s") + "!");
                                }
                            }
                            countdown--;
                        } else {
                            gameStart();
                            invincibilitytimer = 10;
                            invincibility = true;
                            for (NGXplayer p : players) {
                                p.sendMessage("&a&lStarting game...");
                            }
                        }
                    } else {
                        status = "Waiting for players";
                        setCooldown(defaultcooldown);
                    }
                }
            }
        }, 0, 20);
    }

    public NGXenum addPlayer(NGXplayer p) {
        gplayers.put(p, this);
        if (players.size() >= playersmax) {
            queuel.add(p);
            return NGXenum.QUEUEL;
        } else {
            players.add(p);
            for (NGXplayer gp : players) {
                gp.sendMessage("&a" + p.getName() + " &7has joined the game. (&e" + players.size() + "&7/&e" + playersmax + "&7)");
            }
            return NGXenum.CURRENT;
        }
    }

    public void setSpectator(NGXplayer p) {
        if (getAlive().size() == 3) {
            setThird(p);
            game.removeAlive(p);
            p.getPlayer().setGameMode(GameMode.SPECTATOR);
            p.getPlayer().teleport(map.getSpecSpawns().get(0));
        } else if (getAlive().size() == 2) {
            setSecond(p);
            game.removeAlive(p);
            setFirst(game.getAlive().get(0));
            gameEnd();
        } else if (game.getAlive().size() == 1) {
            setFirst(p);
            game.removeAlive(p);
            gameEnd();
        } else {
            game.removeAlive(p);
            p.getPlayer().setGameMode(GameMode.SPECTATOR);
            p.getPlayer().teleport(map.getSpecSpawns().get(0));
        }
    }

    private void prepareMap() {
        map = maps.get(new Random().nextInt(maps.size()));
        map.preloadWorld();
    }

    private void setupPlayers() {
        ingame.addAll(players);
        players.clear();
        if (queuel.size() > 12) {
            for (int i = 11; i >= 0; i--) {
                players.add(queuel.get(i));
                queuel.get(i).remove();
                queuel.get(i).sendMessage("&a&lYou have been added to the game.");
            }
        } else {
            players.addAll(queuel);
            for (NGXplayer p : queuel) {
                p.sendMessage("&a&lYou have been added to the game.");
            }
        }
        for (NGXplayer p : ingame) {
            p.getPlayer().getInventory().clear();
            if (pteam.get(p) == null) {
                NGXteam team = teams.get(new Random().nextInt(teams.size()));
                pteam.put(p, team);
            }

            if (pkit.get(p) == null) {
                pkit.put(p, kits.get(new Random().nextInt(kits.size())));
            }
            NGXkit kit = pkit.get(p);
            if (kit.getMainItem() != null) {
                p.getPlayer().getInventory().clear();
                p.getPlayer().getInventory().setItem(0, CommonUtils.buildItemStack(pkit.get(p).getMainItem(), 1, (short) 0,
                        kit.getMainAbility().getName(), pkit.get(p).getMainAbility().getDesc()));
            }
            if (kit.getSecondaryItem() != null) {
                p.getPlayer().getInventory().setItem(1, CommonUtils.buildItemStack(pkit.get(p).getSecondaryItem(), 1, (short) 0,
                        kit.getSecondaryAbility().getName(), pkit.get(p).getSecondaryAbility().getDesc()));
            }
            if (kit.getPotionEffects() != null) {
                for (PotionEffect potion : kit.getPotionEffects()) {
                    p.getPlayer().addPotionEffect(potion);
                }
            }
            if (kit.getHelmet() != null) p.getPlayer().getInventory().setHelmet(kit.getHelmet());
            if (kit.getChestplate() != null) p.getPlayer().getInventory().setChestplate(kit.getChestplate());
            if (kit.getLeggings() != null) p.getPlayer().getInventory().setLeggings(kit.getLeggings());
            if (kit.getBoots() != null) p.getPlayer().getInventory().setBoots(kit.getBoots());
            for (ItemStack i : kit.getItems()) {
                p.getPlayer().getInventory().addItem(i);
            }
        }
        playing.addAll(ingame);
    }

    private void tpPlayers() {
        map.loadWorld();
        locationtp = 0;
        for (NGXplayer p : ingame) {
            NGXteam team = pteam.get(p);
            if (map.getTeamSpawns(team).size() == 0) {
                p.sendMessage("&4&lError&f: &cNo spawns for your team were set for this map, please report this issue to server administrators.");
                gameEnd();
            } else if (map.getTeamSpawns(team).size() == 1) {
                NGXserver.get().debug(map.getTeamSpawns(team).size() + "");
                p.getPlayer().teleport(map.getTeamSpawns(team).get(0));
            } else {
                p.getPlayer().teleport(map.getTeamSpawns(team).get(locationtp));
                locationtp++;
            }

        }
    }

    private void setupGameevents() {
        for (NGXgameevent ge : gameevents) {
            ge.register();
        }
    }

    private void cleanup() {
        stopGameevents();
        map.destroy();
        map = null;
        for (NGXplayer p : ingame) {
            gplayers.remove(p);
            p.getPlayer().setGameMode(GameMode.SURVIVAL);
            p.getPlayer().getInventory().clear();
            p.getPlayer().getInventory().setHelmet(null);
            p.getPlayer().getInventory().setChestplate(null);
            p.getPlayer().getInventory().setLeggings(null);
            p.getPlayer().getInventory().setBoots(null);
            p.getPlayer().setHealth(20);
            p.getPlayer().setFoodLevel(20);
            p.getPlayer().setExhaustion(0);
            for (PotionEffect effect : p.getPlayer().getActivePotionEffects()) {
                p.getPlayer().removePotionEffect(effect.getType());
            }
        }
        ingame.clear();
        playing.clear();
        NGXgameticker.unregisterTicker(this);
        ListenerManager.unregister(game);
        first = null;
        second = null;
        third = null;
    }

    private void startcleanup() {
        pteam.clear();
        pkit.clear();
    }

    private void stopGameevents() {
        for (NGXgameevent ge : gameevents) {
            ge.unregister();
        }
    }

    public void gameStart() {
        setupPlayers();
        tpPlayers();
        onStart();
        isRunning = true;
        startcleanup();
    }

    public void gameEnd() {
        onEnd();
        cleanup();
        isRunning = false;
    }

    public int getMaxPlayers() {
        return playersmax;
    }

    public int getMinPlayers() {
        return playersmin;
    }

    public String getName() {
        return this.name;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public List<NGXplayer> getAlive() {
        return playing;
    }

    public List<NGXplayer> getParticipating() {
        return ingame;
    }

    public NGXplayer getFirst() {
        return first;
    }

    public void setFirst(NGXplayer first) {
        this.first = first;
    }

    public NGXplayer getSecond() {
        return second;
    }

    public void setSecond(NGXplayer second) {
        this.second = second;
    }

    public NGXplayer getThird() {
        return third;
    }

    public void setThird(NGXplayer third) {
        this.third = third;
    }

    public NGXmap getMap() {
        return map;
    }

    public String getStatus() {
        return status;
    }

    public int getDefaultcooldown() {
        return defaultcooldown;
    }

    public int getCooldown() {
        return countdown;
    }

    public void setCooldown(int time) {
        countdown = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDefaultCooldown(int time) {
        defaultcooldown = time;
    }

    public void removeAlive(NGXplayer p) {
        playing.remove(p);
    }

    public void addKits(NGXkit... list) {
        Collections.addAll(kits, list);
    }

    public void addTeams(NGXteam... list) {
        Collections.addAll(teams, list);
    }

    public void addMaps(NGXmap... list) {
        Collections.addAll(maps, list);
    }

    public void addGameevents(NGXgameevent... list) {
        Collections.addAll(gameevents, list);
    }

    public ArrayList<NGXkit> getKits() {
        return kits;
    }

    public ArrayList<NGXteam> getTeams() {
        return teams;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public boolean isInvincibility() {
        return invincibility;
    }
}
