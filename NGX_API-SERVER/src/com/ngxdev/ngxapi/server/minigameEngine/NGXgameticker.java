package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.interfaces.NGXgametick;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.main.NGXserver;

import java.util.ArrayList;

public class NGXgameticker {
    private static ArrayList<NGXgametick> tickers = new ArrayList<>();

    public static void startTicker(MainClass m) {
        NGXserver.get().getScheduler().scheduleSyncRepeatingTask(m, new Runnable() {
            @Override
            public void run() {
                for (NGXgametick gt : tickers) {
                    gt.onTick();
                }
            }
        }, 0, 2);
    }

    public static void registerTicker(NGXgametick gt) {
        tickers.add(gt);
    }

    public static void unregisterTicker(NGXgametick gt) {
        if (tickers.contains(gt)) tickers.remove(gt);
    }
}
