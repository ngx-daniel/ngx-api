package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.interfaces.NGXgametick;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class NGXability implements Listener, NGXgametick {
    private static ArrayList<NGXability> abilitys = new ArrayList<>();
    public NGXplayer np;
    private HashMap<NGXplayer, Double> pcooldown = new HashMap<>();
    private HashMap<NGXplayer, Integer> puses = new HashMap<>();
    private String name,
            desc;
    private double cooldown;
    private int uses;
    private Listener l;

    public NGXability(String name, String desc, int cooldown, int uses) {
        this.name = name;
        this.desc = desc;
        this.uses = uses;
        this.cooldown = cooldown;
        abilitys.add(this);
        l = ListenerManager.register(this);
        NGXgameticker.registerTicker(this);
    }

    @Override
    public void onTick() {
        for (NGXplayer p : pcooldown.keySet()) {
            double cd = pcooldown.get(p);
            if (cd > 0) {
                pcooldown.put(p, cd - 0.1);
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        this.np = NGXplayer.getNGXplayer(p);
        Action a = e.getAction();
        ItemStack hand = p.getItemInHand();
        if (NGXgame.getPlayerGame(np) == null)
            return;
        if (!(a == Action.RIGHT_CLICK_BLOCK || a == Action.RIGHT_CLICK_AIR))
            return;
        if (!hand.hasItemMeta()) return;
        if (hand.getItemMeta().getDisplayName() == null) return;
        if (!hand.getItemMeta().getDisplayName().equalsIgnoreCase(CommonUtils.convert(getName()))) return;
        if (puses.get(np) == null) puses.put(np, getUses());
        if (!hasUses(np)) {
            np.sendMessage("&4Error&f: &cYou can't use this ability anymore!");
            return;
        }
        if (!isOnCooldown(np)) {
            np.sendMessage("&4Cooldown &c\u00BB &7Reusable in " + new BigDecimal(getPlayerCooldown(np)).setScale(1, BigDecimal.ROUND_HALF_UP) + " seconds.");
            return;
        }
        pcooldown.put(np, cooldown);
        puses.put(np, getPlayerUses(np) - 1);
        exec();
    }

    public int getUses() {
        return uses;
    }

    public int getPlayerUses(NGXplayer p) {
        return puses.get(p);
    }

    public double getCooldown() {
        return cooldown;
    }

    public double getPlayerCooldown(NGXplayer p) {
        if (pcooldown.get(p) != null) return pcooldown.get(p);
        else return 0;
    }

    public boolean isOnCooldown(NGXplayer p) {
        if (!pcooldown.containsKey(p)) {
            pcooldown.put(p, cooldown);
            return true;
        } else {
            return pcooldown.get(p) <= 0.0;
        }
    }

    public boolean hasUses(NGXplayer p) {
        return puses.get(p) == -1 || puses.get(p) != 0;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public Listener getListener() {
        return l;
    }

    public abstract void exec();
}
