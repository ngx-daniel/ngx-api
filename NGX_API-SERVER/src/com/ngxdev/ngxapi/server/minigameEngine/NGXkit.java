package com.ngxdev.ngxapi.server.minigameEngine;

import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

public class NGXkit {
    private static boolean instance = false;
    private static ArrayList<NGXkit> kits = new ArrayList<>();
    private Material mainitem,
            secondaryitem;
    private NGXability mainability,
            secondaryability;
    private String name;
    private Material mat;
    private List<ItemStack> items = new ArrayList<>();
    private ItemStack helmet;
    private ItemStack chestplate;
    private ItemStack leggings;
    private ItemStack boots;
    private int cost;
    private PotionEffect[] potioneffects;

    public NGXkit(String name, Material mat, int cost) {
        if (!instance) {
            instance = true;
            this.name = CommonUtils.convert(name);
            this.cost = cost;
            this.mat = mat;
            this.cost = cost;
            kits.add(this);
        }
    }

    public static NGXkit matchKit(String kit) {
        for (NGXkit m : kits) {
            if (m.getName().equalsIgnoreCase(kit)) {
                return m;
            }
        }
        return null;
    }

    public void addItem(ItemStack item) {
        items.add(item);
    }

    public List<ItemStack> getItems() {
        return items;
    }

    public Material getMaterial() {
        return mat;
    }

    public int getCost() {
        return cost;
    }

    public String getName() {
        return name;
    }

    public Material getMainItem() {
        return mainitem;
    }

    public Material getSecondaryItem() {
        return secondaryitem;
    }

    public NGXability getMainAbility() {
        return mainability;
    }

    public NGXability getSecondaryAbility() {
        return secondaryability;
    }

    public PotionEffect[] getPotionEffects() {
        return potioneffects;
    }

    public void setPotionEffects(PotionEffect... potions) {
        potioneffects = potions;
    }

    public ItemStack getBoots() {
        return boots;
    }

    public void setBoots(ItemStack boots) {
        this.boots = boots;
    }

    public ItemStack getLeggings() {
        return leggings;
    }

    public void setLeggings(ItemStack leggings) {
        this.leggings = leggings;
    }

    public ItemStack getChestplate() {
        return chestplate;
    }

    public void setChestplate(ItemStack chestplate) {
        this.chestplate = chestplate;
    }

    public ItemStack getHelmet() {
        return helmet;
    }

    public void setHelmet(ItemStack helmet) {
        this.helmet = helmet;
    }

    public void setMain(Material mat, NGXability ability) {
        mainitem = mat;
        mainability = ability;
    }

    public void setSecondary(Material mat, NGXability ability) {
        secondaryitem = mat;
        secondaryability = ability;
    }
}
