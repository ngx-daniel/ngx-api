package com.ngxdev.ngxapi.server.minigameEngine;

import org.bukkit.ChatColor;

public enum NGXteam {
    ORANGE("Orange", (short) 1, ChatColor.GOLD),
    MAGENTA("Magenta", (short) 2, ChatColor.LIGHT_PURPLE),
    LIGHT_BLUE("Light Blue", (short) 3, ChatColor.BLUE),
    YELLOW("Yellow", (short) 4, ChatColor.YELLOW),
    LIME("Lime", (short) 5, ChatColor.GREEN),
    PINK("Pink", (short) 6, ChatColor.WHITE),
    GRAY("Gray", (short) 7, ChatColor.GRAY),
    SILVER("Silver", (short) 8, ChatColor.DARK_GRAY),
    CYAN("Cyan", (short) 9, ChatColor.AQUA),
    PURPLE("Purple", (short) 10, ChatColor.DARK_PURPLE),
    BLUE("Blue", (short) 11, ChatColor.DARK_BLUE),
    BROWN("Brown", (short) 12, ChatColor.DARK_AQUA),
    GREEN("Green", (short) 13, ChatColor.DARK_GREEN),
    RED("Red", (short) 14, ChatColor.DARK_RED),
    BLACK("Black", (short) 15, ChatColor.BLACK);

    String name;
    short data;
    ChatColor color;

    NGXteam(String name, short data, ChatColor color) {
        this.name = name;
        this.data = data;
        this.color = color;
    }

    public static NGXteam matchTeam(String team) {
        for (NGXteam t : NGXteam.values()) {
            if (t.getName().equalsIgnoreCase(team)) return t;
        }
        return null;
    }

    public short getData() {
        return data;
    }

    public String getName() {
        return getColor() + "&l" + name;
    }

    public ChatColor getColor() {
        return color;
    }
}
