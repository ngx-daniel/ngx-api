package com.ngxdev.ngxapi.server.NMS;

import com.ngxdev.ngxapi.server.utils.PacketUtils;
import org.bukkit.Location;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class v1_8_R1 extends FakeWither {
    private Object wither;
    private int id;

    public v1_8_R1(String name, Location loc) {
        super(name, loc);
    }

    @Override
    public Object getSpawnPacket() {
        Class<?> Entity = PacketUtils.getCraftClass("Entity");
        Class<?> EntityLiving = PacketUtils.getCraftClass("EntityLiving");
        Class<?> EntityWither = PacketUtils.getCraftClass("EntityWither");
        Object packet = null;

        try {
            wither = EntityWither.getConstructor(PacketUtils.getCraftClass("World")).newInstance(getWorld());

            Method setLocation = PacketUtils.getMethod(EntityWither, "setLocation", new Class<?>[]{double.class, double.class, double.class, float.class, float.class});
            setLocation.invoke(wither, getX(), getY(), getZ(), getPitch(), getYaw());

            Method setInvisible = PacketUtils.getMethod(EntityWither, "setInvisible", new Class<?>[]{boolean.class});
            setInvisible.invoke(wither, true);

            Method setCustomName = PacketUtils.getMethod(EntityWither, "setCustomName", new Class<?>[]{String.class});
            setCustomName.invoke(wither, name);

            Method setHealth = PacketUtils.getMethod(EntityWither, "setHealth", new Class<?>[]{float.class});
            setHealth.invoke(wither, health);

            Field motX = PacketUtils.getField(Entity, "motX");
            motX.set(wither, getXvel());

            Field motY = PacketUtils.getField(Entity, "motY");
            motY.set(wither, getYvel());

            Field motZ = PacketUtils.getField(Entity, "motZ");
            motZ.set(wither, getZvel());

            Method getId = PacketUtils.getMethod(EntityWither, "getId", new Class<?>[]{});
            this.id = (Integer) getId.invoke(wither);

            Class<?> PacketPlayOutSpawnEntityLiving = PacketUtils.getCraftClass("PacketPlayOutSpawnEntityLiving");

            packet = PacketPlayOutSpawnEntityLiving.getConstructor(new Class<?>[]{EntityLiving}).newInstance(wither);
        } catch (IllegalArgumentException | SecurityException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return packet;
    }

    @Override
    public Object getDestroyPacket() {
        Class<?> PacketPlayOutEntityDestroy = PacketUtils.getCraftClass("PacketPlayOutEntityDestroy");
        Object packet;
        try {
            packet = PacketPlayOutEntityDestroy.newInstance();
            Field a = PacketPlayOutEntityDestroy.getDeclaredField("a");
            a.setAccessible(true);
            a.set(packet, id);
            return packet;
        } catch (SecurityException | NoSuchFieldException | IllegalAccessException | InstantiationException | IllegalArgumentException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Object getMetaPacket(Object watcher) {
        Class<?> DataWatcher = PacketUtils.getCraftClass("DataWatcher");

        Class<?> PacketPlayOutEntityMetadata = PacketUtils.getCraftClass("PacketPlayOutEntityMetadata");

        Object packet = null;
        try {
            packet = PacketPlayOutEntityMetadata.getConstructor(new Class<?>[]{int.class, DataWatcher, boolean.class}).newInstance(id, watcher, true);
        } catch (IllegalArgumentException | SecurityException | InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return packet;
    }

    @Override
    public Object getTeleportPacket(Location loc) {
        Class<?> PacketPlayOutEntityTeleport = PacketUtils.getCraftClass("PacketPlayOutEntityTeleport");
        Object packet = null;

        try {
            packet = PacketPlayOutEntityTeleport.getConstructor(new Class<?>[]{int.class, int.class, int.class, int.class, byte.class, byte.class, boolean.class}).newInstance(this.id, loc.getBlockX() * 32, loc.getBlockY() * 32, loc.getBlockZ() * 32, (byte) ((int) loc.getYaw() * 256 / 360), (byte) ((int) loc.getPitch() * 256 / 360), false);
        } catch (IllegalArgumentException | SecurityException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return packet;
    }

    @Override
    public Object getWatcher() {
        Class<?> Entity = PacketUtils.getCraftClass("Entity");
        Class<?> DataWatcher = PacketUtils.getCraftClass("DataWatcher");

        Object watcher = null;
        try {
            watcher = DataWatcher.getConstructor(new Class<?>[]{Entity}).newInstance(wither);
            Method a = PacketUtils.getMethod(DataWatcher, "a", new Class<?>[]{int.class, Object.class});

            a.invoke(watcher, 5, 0x20);
            a.invoke(watcher, 6, health);
            a.invoke(watcher, 7, 0);
            a.invoke(watcher, 8, (byte) 0);
            a.invoke(watcher, 10, name);
            a.invoke(watcher, 11, (byte) 1);
        } catch (IllegalArgumentException | SecurityException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        return watcher;
    }
}
