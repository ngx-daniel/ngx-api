package com.ngxdev.ngxapi.server.utils;

import com.ngxdev.ngxapi.server.events.PluginMessageReceiveEvent;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.sockets.SocketDecoder;
import org.bukkit.Bukkit;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class SocketUtils {
    public static boolean active = false;
    private static MainClass m;
    private static int masterport,
            port;
    private static ServerSocket ss;
    private static String password,
            masterip;
    private static int attempt = 0;

    public static void setup() {
        m = MainClass.getMain();
        m.getLogger().info("Setting up socked utils.");
        m.config.build("socket.password", "password");
        setPort();
        m.config.build("socket.bungeeip", "localhost");
        m.config.build("socket.bungeeport", 1337);
        masterport = m.config.getInt("socket.bungeeport");
        port = m.config.getInt("socket.port");
        password = m.config.getString("socket.password");
        masterip = m.config.getString("socket.bungeeip");
        SocketUtils.openServerSocket(port);
        sendPort();
        SocketUtils.sendData("servers", "update", "bungee");
        active = true;
    }

    public static void setupDefaultDecoder() {
        new SocketDecoder();
    }

    private static void openServerSocket(final int port) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ss = new ServerSocket(port);
                    while (true) {
                        Socket s;
                        try {
                            s = ss.accept();
                        } catch (SocketException e) {
                            return;
                        }
                        decode(new DataInputStream(s.getInputStream()).readUTF());
                    }
                } catch (BindException e) {
                    if (attempt <= 10) {
                        openServerSocket(CommonUtils.randomInt(10000, 20000));
                        attempt++;
                    } else {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void close() {
        try {
            ss.close();
        } catch (Exception ignored) {
        }
    }

    public static void sendData(String data, String channel, String server) {
        Socket client;
        try {
            client = new Socket(masterip, masterport);
            DataOutputStream ds = new DataOutputStream(client.getOutputStream());
            ds.writeUTF(compile(password, server, channel, data));
            ds.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String compile(String password, String server, String channel, String data) {
        return password + "|" + server + "|" + channel + "|" + data.replace(" ", "_");
    }

    private static void decode(String query) {
        String[] args = query.split("\\|");
        String channel = args[0];
        String info = args[1].replace("_", " ");
        PluginMessageReceiveEvent event = new PluginMessageReceiveEvent(channel, info.replace("_", " "));
        Bukkit.getServer().getPluginManager().callEvent(event);
    }

    private static void setPort() {
        if (m.config.get("socket.port") == null) {
            int port = CommonUtils.randomInt(10000, 20000);
            m.config.set("socket.port", port);
        }
    }

    private static void sendPort() {
        SocketUtils.sendData(m.getServer().getPort() + ":" + port, "setport", "bungee");
        m.getLogger().info("Sending heartbeat.");
    }
}
