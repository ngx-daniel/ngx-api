package com.ngxdev.ngxapi.server.utils;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.*;

public class CommonUtils {
    public static final float RADTODEG = 57.29577951F;
    static final float DEGTORAD = 0.017453293F;

    public static String getArgs(String[] args, int num) {
        String output = "";
        for (int i = num; i < args.length; i++) {
            if (output.equals("")) output = args[i];
            else output = output + " " + args[i];
        }
        return output;
    }

    public static String convert(String s) {
        if (s != null) return ChatColor.translateAlternateColorCodes('&', s);
        else return null;
    }

    public static String convert(String s, Player p) {
        if (s != null) return ChatColor.translateAlternateColorCodes('&', s).replace("%player%", p.getName());
        else return null;
    }

    public static String strip(String s) {
        return ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', s));
    }

    public static List<String> convert(String... list) {
        List<String> list2 = new ArrayList<>();
        for (String s : list) {
            list2.add(org.bukkit.ChatColor.translateAlternateColorCodes('&', s));
        }
        return list2;
    }

    public static List<String> convert(List<String> list) {
        List<String> list2 = new ArrayList<>();
        for (String s : list) {
            list2.add(org.bukkit.ChatColor.translateAlternateColorCodes('&', s));
        }
        return list2;
    }

    public static float getLookAtYaw(Entity loc, Entity lookat) {
        return getLookAtYaw(loc.getLocation(), lookat.getLocation());
    }

    public static float getLookAtYaw(Block loc, Block lookat) {
        return getLookAtYaw(loc.getLocation(), lookat.getLocation());
    }

    public static float getLookAtYaw(Location loc, Location lookat) {
        return getLookAtYaw(lookat.getX() - loc.getX(), lookat.getZ() - loc.getZ());
    }

    public static float getLookAtYaw(Vector motion) {
        return getLookAtYaw(motion.getX(), motion.getZ());
    }

    public static float getLookAtYaw(double dx, double dz) {
        float yaw = 0;
        if (dx != 0) {
            if (dx < 0) {
                yaw = 270;
            } else {
                yaw = 90;
            }
            yaw -= atan(dz / dx);
        } else if (dz < 0) {
            yaw = 180;
        }
        return -yaw - 90;
    }

    public static float getLookAtPitch(double motY, double motXZ) {
        return -atan(motY / motXZ);
    }

    public static float atan(double value) {
        return RADTODEG * (float) Math.atan(value);
    }

    public static Location getfromloc(Location loc, int offset) {
        return loc.add(loc.getDirection().multiply(offset));
    }

    public static Location move(Location loc, Vector offset) {
        return move(loc, offset.getX(), offset.getY(), offset.getZ());
    }

    public static Location move(Location loc, double dx, double dy, double dz) {
        Vector off = rotate(loc.getYaw(), loc.getPitch(), dx, dy, dz);
        double x = loc.getX() + off.getX();
        double y = loc.getY() + off.getY();
        double z = loc.getZ() + off.getZ();
        return new Location(loc.getWorld(), x, y, z, loc.getYaw(), loc.getPitch());
    }

    public static Vector rotate(float yaw, float pitch, Vector value) {
        return rotate(yaw, pitch, value.getX(), value.getY(), value.getZ());
    }

    public static Vector rotate(float yaw, float pitch, double x, double y, double z) {
        float angle;
        angle = yaw * DEGTORAD;
        double sinyaw = Math.sin(angle);
        double cosyaw = Math.cos(angle);

        angle = pitch * DEGTORAD;
        double sinpitch = Math.sin(angle);
        double cospitch = Math.cos(angle);

        double newx = 0.0;
        double newy = 0.0;
        double newz = 0.0;
        newz -= x * cosyaw;
        newz -= y * sinyaw * sinpitch;
        newz -= z * sinyaw * cospitch;
        newx += x * sinyaw;
        newx -= y * cosyaw * sinpitch;
        newx -= z * cosyaw * cospitch;
        newy += y * cospitch;
        newy -= z * sinpitch;

        return new Vector(newx, newy, newz);
    }

    public static int randomInt(int min, int max) {
        return (int) (Math.random() * (max - min)) + min;
    }

    public static double randomDouble(double min, double max) {
        Random r = new Random();
        return min + (max - min) * r.nextDouble();
    }

    @Deprecated
    public static ItemStack buildCommandPaper1_7(String query) {
        String[] args = query.split(" ");
        String cmd = args[0].replace("_", " ");
        String name = getAttribute(query, "name:");
        ItemStack item = new ItemStack(339);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(CommonUtils.convert("&a|CMD| &7" + name));
        meta.setLore(Collections.singletonList(CommonUtils.convert("&0&k" + cmd)));
        meta.addEnchant(Enchantment.DURABILITY, 1, true);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack buildCommandPaper(String query) {
        String[] args = query.split(" ");
        String cmd = args[0].replace("_", " ");
        String name = getAttribute(query, "name:");
        ItemStack item = new ItemStack(Material.PAPER);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(CommonUtils.convert("&a|CMD| &7" + name));
        meta.setLore(Collections.singletonList(CommonUtils.convert("&0&k" + cmd)));
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS);
        meta.addEnchant(Enchantment.DURABILITY, 1, true);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack buildItemStack(Material mat, int amount, short data, String name) {
        ItemStack item = new ItemStack(mat, amount, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(CommonUtils.convert(name));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack buildItemStack(Material mat, int amount, short data, String name, String lore) {
        ItemStack item = new ItemStack(mat, amount, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(CommonUtils.convert(name));
        meta.setLore(Collections.singletonList(CommonUtils.convert(lore)));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack buildItemStack(Material mat, int amount, short data, String name, ArrayList<String> lore) {
        ItemStack item = new ItemStack(mat, amount, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(CommonUtils.convert(name));
        meta.setLore(CommonUtils.convert(lore));
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack buildItemStack(Material mat, int amount, short data, String name, List<String> lore, HashMap<Enchantment, Integer> ench) {
        ItemStack item = new ItemStack(mat, amount, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(CommonUtils.convert(name));
        meta.setLore(CommonUtils.convert(lore));
        for (Enchantment e : ench.keySet()) {
            meta.addEnchant(e, ench.get(e), true);
        }
        item.setItemMeta(meta);
        return item;
    }

    @Deprecated
    public static ItemStack buildItemStackFromID(String s) {
        String[] args = s.split(" ");
        Integer id = Integer.parseInt(args[0]);
        int amount = Integer.parseInt(args[1]);
        Short data = Short.parseShort(args[2]);
        String name = getAttribute(s, "name:");
        String cost = getAttribute(s, "cost:");
        List<String> lore = getAttribute(s, "lore:") != null ? Arrays.asList(getAttribute(s, "lore:").split("\\|")) : new ArrayList<String>();
        Map<Enchantment, Integer> enchantments = getEnchantments(s);
        if (cost != null) lore.add(CommonUtils.convert("&7Price: " + cost));

        ItemStack item = new ItemStack(id, amount, data);
        ItemMeta meta = item.getItemMeta();
        if (name != null) meta.setDisplayName(name);
        if (!lore.isEmpty()) meta.setLore(lore);
        if (!enchantments.isEmpty())
            for (Enchantment ench : enchantments.keySet()) meta.addEnchant(ench, enchantments.get(ench), true);
        item.setItemMeta(meta);
        return item;
    }

    public static ItemStack buildItemStack(String s) {
        String[] args = s.split(" ");
        Material mat = Material.matchMaterial(args[0].toUpperCase());
        int amount = Integer.parseInt(args[1]);
        Short data = Short.parseShort(args[2]);
        String name = getAttribute(s, "name:");
        String cost = getAttribute(s, "cost:");
        List<String> lore = getAttribute(s, "lore:") != null ? Arrays.asList(getAttribute(s, "lore:").split("\\|")) : new ArrayList<String>();
        Map<Enchantment, Integer> enchantments = getEnchantments(s);
        List<ItemFlag> flags = getItemFlags(s);
        if (cost != null) lore.add(CommonUtils.convert("&7Price: " + cost));

        ItemStack item = new ItemStack(mat, amount, data);
        ItemMeta meta = item.getItemMeta();
        if (name != null) meta.setDisplayName(name);
        if (!lore.isEmpty()) meta.setLore(lore);
        if (!enchantments.isEmpty())
            for (Enchantment ench : enchantments.keySet()) meta.addEnchant(ench, enchantments.get(ench), true);
        if (!flags.isEmpty())
            for (ItemFlag f : flags)
                meta.addItemFlags(f);
        item.setItemMeta(meta);
        return item;
    }

    public static String getAttribute(String query, String attr) {
        String[] pieces = query.split(" ");
        for (String piece : pieces) {
            if (piece.startsWith(attr)) {
                return CommonUtils.convert(piece.split(":")[1].replace(attr, "").replace("_", " ").replace("%1", "_").replace("%2", ":"));
            }
        }
        return null;
    }

    public static List<ItemFlag> getItemFlags(String query) {
        List<ItemFlag> flags = new ArrayList<>();
        String[] args = query.split(" ");
        for (String arg : args)
            for (ItemFlag flag : ItemFlag.values()) if (flag.name().equalsIgnoreCase(arg)) flags.add(flag);
        return flags;
    }

    public static Map<Enchantment, Integer> getEnchantments(String query) {
        Map<Enchantment, Integer> enchantments = new HashMap<>();
        Map<String, String> replacemap = new HashMap<>();
        replacemap.put("sharpness", "DAMAGE_ALL");
        String[] args = query.split(" ");
        for (String arg : args) {
            String[] s = arg.split(":");
            for (String rv : replacemap.keySet()) {
                s[0] = s[0].replaceAll("/" + rv + "/i", replacemap.get(rv));
            }
            if (Enchantment.getByName(s[0].toUpperCase()) != null) {
                enchantments.put(Enchantment.getByName(s[0].toUpperCase()), Integer.parseInt(s[1]));
            }
        }
        return enchantments;
    }

    public static Location getRandomLocation(int min, int max, World world) {
        int x = CommonUtils.randomInt(-max, max);
        int z;
        if (Math.abs(x) < min) {
            z = CommonUtils.randomInt(min, max);
            if (new Random().nextBoolean()) {
                z = -z;
            }
        } else {
            z = CommonUtils.randomInt(-max, max);
        }
        int y = 255;
        while (world.getBlockAt(x, y, z).getType() == Material.AIR) {
            y--;
        }
        return world.getBlockAt(x, y + 1, z).getLocation();
    }

    public static Location getRandomLocation(Location center, int min, int max, World world) {
        int x = CommonUtils.randomInt(-max, max);
        int z;
        if (Math.abs(x) < min) {
            z = CommonUtils.randomInt(min, max);
            if (new Random().nextBoolean()) {
                z = -z;
            }
        } else {
            z = CommonUtils.randomInt(-max, max);
        }
        x = center.getBlockX() + x;
        z = center.getBlockZ() + z;
        int y = 255;
        while (world.getBlockAt(x, y, z).getType() == Material.AIR) {
            y--;
        }
        return world.getBlockAt(x, y + 1, z).getLocation();
    }

    public static double getPrecent(int percent, double value) {
        return value * (percent / 100.0f);
    }

    public static String toBar(String o, int bars, int max) {
        String output = "&a";

        for (int bar = 0; bar < max; bar++) {
            if (bar == bars) output += "&7" + o;
            else output += o;
        }
        return output;
    }

    public static String toObjectString(String o, int amount) {
        String output = "";
        for (int bar = 0; bar < amount; bar++) {
            output += o;
        }
        return output;
    }
}
