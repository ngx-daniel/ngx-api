package com.ngxdev.ngxapi.server.utils;

import com.ngxdev.ngxapi.server.API.Config;
import com.ngxdev.ngxapi.server.main.MainClass;
import org.bukkit.Location;
import org.bukkit.World;

public class LocationUtils {
    public static Location buildFromConfig(String path, Config cfg) {
        World w = MainClass.getMain().getServer().getWorld(cfg.getString(path + ".world"));
        double x = cfg.getDouble(path + ".x");
        double y = cfg.getDouble(path + ".y");
        double z = cfg.getDouble(path + ".z");
        float pitch = cfg.getInt(path + ".pitch");
        float yaw = cfg.getInt(path + ".yaw");
        return new Location(w, x, y, z, yaw, pitch);
    }

    public static void buildToConfig(Location loc, String path, Config cfg) {
        cfg.set(path + ".world", loc.getWorld().getName());
        cfg.set(path + ".x", loc.getX());
        cfg.set(path + ".y", loc.getY());
        cfg.set(path + ".z", loc.getZ());
        cfg.set(path + ".pitch", loc.getPitch());
        cfg.set(path + ".yaw", loc.getYaw());
    }
}
