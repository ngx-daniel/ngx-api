package com.ngxdev.ngxapi.server.utils;

import java.io.File;

public class NGXfileUtils {
    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    file.delete();
                }
            }
        }
        return (path.delete());
    }

    public static boolean deleteFile(File path) {
        return path.delete();
    }
}
