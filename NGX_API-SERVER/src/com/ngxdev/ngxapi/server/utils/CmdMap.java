package com.ngxdev.ngxapi.server.utils;

import com.ngxdev.ngxapi.server.main.MainClass;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.SimplePluginManager;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class CmdMap {
    public static PluginCommand getCommand(String name, Plugin plugin) {
        PluginCommand command = null;
        try {
            Constructor<PluginCommand> c = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            c.setAccessible(true);

            command = c.newInstance(name, plugin);
        } catch (IllegalAccessException | SecurityException | InvocationTargetException | NoSuchMethodException | IllegalArgumentException | InstantiationException e) {
            e.printStackTrace();
        }
        return command;
    }

    public static CommandMap getCommandMap() {
        CommandMap commandMap = null;

        try {
            if (Bukkit.getPluginManager() instanceof SimplePluginManager) {
                Field f = SimplePluginManager.class.getDeclaredField("commandMap");
                f.setAccessible(true);

                commandMap = (org.bukkit.command.CommandMap) f.get(Bukkit.getPluginManager());
            }
        } catch (NoSuchFieldException | SecurityException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
        }
        return commandMap;
    }

    public static SimpleCommandMap getSimpleCommandMap() {
        SimplePluginManager spm = (SimplePluginManager) MainClass.getMain().getServer().getPluginManager();

        try {
            Field f = SimplePluginManager.class.getDeclaredField("commandMap");
            f.setAccessible(true);
            return (SimpleCommandMap) f.get(spm);
        } catch (NoSuchFieldException | IllegalAccessException ignored) {
        }

        return null;
    }

    private static Object getPrivateField(Object object, String field) throws SecurityException,
            NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Field objectField = clazz.getDeclaredField(field);
        objectField.setAccessible(true);
        Object result = objectField.get(object);
        objectField.setAccessible(false);
        return result;
    }

    public static void unregister(String command) {
        try {
            PluginCommand cmd = MainClass.getMain().getCommand(command);
            if (cmd != null) {
                cmd.unregister(getSimpleCommandMap());
                cmd.getAliases().clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
