package com.ngxdev.ngxapi.server.utils;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.ngxdev.ngxapi.server.main.MainClass;
import org.bukkit.entity.Player;

public class ServerUtils {
    public static void sendPlayer(final Player p, final String server) {
        p.sendMessage(CommonUtils.convert("&7Connecting..."));
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        p.sendPluginMessage(MainClass.getMain(), "BungeeCord", out.toByteArray());
    }
}
