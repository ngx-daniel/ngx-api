package com.ngxdev.ngxapi.server.utils;

import com.ngxdev.ngxapi.server.main.MainClass;

public class TickUtils implements Runnable {
    public static int TICK_COUNT = 0;
    public static long[] TICKS = new long[600];
    public static long LAST_TICK = 0L;

    public static void setup() {
        MainClass m = MainClass.getMain();
        m.getServer().getScheduler().scheduleSyncRepeatingTask(m, new TickUtils(), 100L, 1L);
    }

    public static double getTPS() {
        return getTPS(600);
    }

    public static double getTPS(int ticks) {
        int target = (TICK_COUNT - 1 - ticks) % TICKS.length;
        long elapsed = System.currentTimeMillis() - TICKS[target];

        return ticks / (elapsed / 1000.0D);
    }

    public static long getElapsed(int tickID) {
        if (TICK_COUNT - tickID >= TICKS.length) {
        }

        long time = TICKS[(tickID % TICKS.length)];
        return System.currentTimeMillis() - time;
    }

    public void run() {
        TICKS[(TICK_COUNT % TICKS.length)] = System.currentTimeMillis();
        TICK_COUNT += 1;
    }
}
