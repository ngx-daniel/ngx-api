package com.ngxdev.ngxapi.server.utils;

import java.math.BigDecimal;

public class TimeUtils {
    public static boolean elapsed(final long from, final long required) {
        return System.currentTimeMillis() - from > required;
    }

    public static String milisToTime(Long milis) {
        double seconds = milis / 1000;
        double minutes = seconds / 60;
        double hours = minutes / 60;
        double days = hours / 24;
        double weeks = days / 7;
        double months = days / 31;
        double years = months / 12;
        if (years >= 1) {
            return new BigDecimal(years).setScale(1, BigDecimal.ROUND_HALF_DOWN) + " year" + (years >= 1 ? "s" : "");
        } else if (months >= 1) {
            return new BigDecimal(months).setScale(1, BigDecimal.ROUND_HALF_DOWN) + " month" + (months >= 1 ? "s" : "");
        } else if (weeks >= 1) {
            return new BigDecimal(weeks).setScale(1, BigDecimal.ROUND_HALF_DOWN) + " week" + (weeks >= 1 ? "s" : "");
        } else if (days >= 1) {
            return new BigDecimal(days).setScale(1, BigDecimal.ROUND_HALF_DOWN) + " day" + (days >= 1 ? "s" : "");
        } else if (hours >= 1) {
            return new BigDecimal(hours).setScale(1, BigDecimal.ROUND_HALF_DOWN) + " hour" + (hours >= 1 ? "s" : "");
        } else if (minutes >= 1) {
            return new BigDecimal(minutes).setScale(1, BigDecimal.ROUND_HALF_DOWN) + " minute" + (minutes >= 1 ? "s" : "");
        } else {
            return new BigDecimal(seconds).setScale(1, BigDecimal.ROUND_HALF_DOWN) + " second" + (seconds >= 1 ? "s" : "");
        }
    }

    public static String milisToDHMS(Long milis) {
        int seconds = milis.intValue() / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;
        int days = hours / 24;
        return days % 31 + ":" + hours % 24 + ":" + minutes % 60 + ":" + seconds % 60;
    }
}
