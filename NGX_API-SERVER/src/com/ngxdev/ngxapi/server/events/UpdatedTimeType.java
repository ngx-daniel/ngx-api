package com.ngxdev.ngxapi.server.events;

import com.ngxdev.ngxapi.server.utils.TimeUtils;

public enum UpdatedTimeType {
    MIN64(3840000L),
    MIN32(1920000L),
    MIN16(960000L),
    MIN08(480000L),
    MIN04(240000L),
    MIN02(120000L),
    MIN01(60000L),
    SECOND(200L),
    NONA(180L),
    OCTA(160L),
    HEPTA(140L),
    HEXA(120L),
    PENTA(100L),
    QUAD(80L),
    TRIPLE(60L),
    DOUBLE(40L),
    TICK(20L);

    private long time;
    private long last;

    UpdatedTimeType(long time) {
        this.time = time;
        this.last = System.currentTimeMillis();
    }

    public boolean passed() {
        if (TimeUtils.elapsed(this.last, this.time)) {
            this.last = System.currentTimeMillis();
            return true;
        }
        return false;
    }
}
