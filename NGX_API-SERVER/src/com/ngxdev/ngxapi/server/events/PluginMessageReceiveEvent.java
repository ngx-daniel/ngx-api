package com.ngxdev.ngxapi.server.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PluginMessageReceiveEvent extends Event {
    private static HandlerList handlers;

    static {
        handlers = new HandlerList();
    }

    private String channel,
            message;

    public PluginMessageReceiveEvent(String channel, String data) {
        this.channel = channel;
        this.message = data;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

}
