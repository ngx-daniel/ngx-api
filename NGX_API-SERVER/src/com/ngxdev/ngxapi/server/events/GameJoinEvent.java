package com.ngxdev.ngxapi.server.events;

import com.ngxdev.ngxapi.server.minigameEngine.NGXgame;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GameJoinEvent extends Event {
    private static HandlerList handlers;

    static {
        handlers = new HandlerList();
    }

    NGXgame game;
    Player p;

    public GameJoinEvent(Player p, NGXgame game) {
        this.game = game;
        this.p = p;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Player getPlayer() {
        return p;
    }

    public NGXgame getGame() {
        return game;
    }

    public HandlerList getHandlers() {
        return handlers;
    }
}
