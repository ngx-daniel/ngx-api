package com.ngxdev.ngxapi.server.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public final class ServerPredisableEvent extends Event {
    private static HandlerList handlers;

    static {
        handlers = new HandlerList();
    }

    public ServerPredisableEvent() {
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

}
