package com.ngxdev.ngxapi.server.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class UpdateEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private UpdatedTimeType type;

    public UpdateEvent(UpdatedTimeType example) {
        this.type = example;
    }

    public static HandlerList getHandlerList() {
        return UpdateEvent.handlers;
    }

    public UpdatedTimeType getType() {
        return this.type;
    }

    public HandlerList getHandlers() {
        return UpdateEvent.handlers;
    }
}
