package com.ngxdev.ngxapi.server.database;


import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.main.NGXserver;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.event.Listener;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQL implements Listener {
    private static SQL instance;

    private String host;
    private int port;
    private String user;
    private String password;
    private String database;
    private Connection con;

    public SQL() {
        MainClass m = MainClass.getMain();
        m.config.build("sql.host", "localhost");
        m.config.build("sql.port", 3306);
        m.config.build("sql.user", "ngxapi");
        m.config.build("sql.password", "password");
        m.config.build("sql.database", "ngxapi");
        host = m.config.getString("sql.host");
        port = m.config.getInt("sql.port");
        user = m.config.getString("sql.user");
        password = m.config.getString("sql.password");
        database = m.config.getString("sql.database");
        openConnection();
        createTable("ngxapi_data");
    }

    public static void setup() {
        instance = new SQL();
    }

    public static SQL getInstance() {
        return SQL.instance;
    }

    public static List<String> getColumns(ResultSet rs) throws SQLException {
        List<String> list = new ArrayList<>();
        ResultSetMetaData rsMetaData = rs.getMetaData();
        int numberOfColumns = rsMetaData.getColumnCount();

        for (int i = 1; i < numberOfColumns + 1; i++) {
            list.add(rsMetaData.getColumnName(i));
        }
        return list;
    }

    public String getDatabase() {
        return database;
    }

    public ResultSet sendQuery(String query) {
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (con == null) openConnection();
            st = con.prepareStatement(query);
            rs = st.executeQuery();
        } catch (SQLException e) {
            System.err.println("Failed to send update " + query + ".");
            e.printStackTrace();
            return null;
        } finally {
            closeResources(null, st);
        }
        closeResources(null, st);
        return rs;
    }

    public ResultSet sendUpdateQuery(String query) {
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            if (con == null) openConnection();
            st = con.prepareStatement(query);
            st.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Failed to send update " + query + ".");
            e.printStackTrace();
            return null;
        } finally {
            closeResources(null, st);
        }
        closeResources(null, st);
        return rs;
    }

    public void createTable(String table) {
        sendUpdateQuery("CREATE TABLE IF NOT EXISTS " + table + " (ID SERIAL PRIMARY KEY, info VARCHAR(100))");
    }

    public void createColumn(String table, String name) {
        try {
            if (con == null) openConnection();
            DatabaseMetaData md = con.getMetaData();
            ResultSet rs = md.getColumns(null, null, table, name);
            if (!rs.next()) {
                sendUpdateQuery("ALTER TABLE " + table + " ADD \"" + name + "\" VARCHAR(100)");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkExist(String table, String info) {
        final String query = "SELECT EXISTS(SELECT 1 FROM " + table + " WHERE info = ?)";
        try (PreparedStatement st = con.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.RETURN_GENERATED_KEYS)) {
            st.setString(1, info);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    return rs.getBoolean(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void createData(String table, String source, String data, String value) {
        createColumn(table, data);
        if (getData(table, source, data) == null) {
            sendUpdateQuery("UPDATE " + table + " SET \"" + data + "\"='" + value + "' WHERE info='" + source + "'");
        }
    }

    public void setData(String table, String source, String data, String value) {
        createColumn(table, data);
        sendUpdateQuery("UPDATE " + table + " SET \"" + data + "\"='" + value + "' WHERE info='" + source + "'");
    }

    public String getData(String table, String source, String data) {
        ResultSet rs = null;
        PreparedStatement st = null;
        String field = null;
        try {
            st = con.prepareStatement("SELECT * FROM " + table + " WHERE info=?", ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, source);
            rs = st.executeQuery();
            rs.last();
            if (rs.getRow() != 0) {
                rs.first();
                field = rs.getString(data);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            closeResources(rs, st);
        }
        closeResources(rs, st);
        return field;
    }

    public Map<String, String> getFields(String table, String source) {
        Map<String, String> map = new HashMap<>();
        ResultSet rs = null;
        PreparedStatement st = null;
        try {
            st = this.con.prepareStatement("SELECT * FROM " + table + " WHERE info=?", ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, source);
            rs = st.executeQuery();
            for (String colum : getColumns(rs)) {
                rs.first();
                if (rs.getRow() != 0) {
                    map.put(colum, rs.getString(colum));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(rs, st);
        }
        closeResources(rs, st);
        return map;
    }

    public Map<String, String> getFieldsWithAttr(String table, String source, String attr) {
        Map<String, String> map = new HashMap<>();
        ResultSet rs = null;
        PreparedStatement st = null;
        try {
            st = this.con.prepareStatement("SELECT * FROM " + table + " WHERE info=?", ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.RETURN_GENERATED_KEYS);
            st.setString(1, source);
            rs = st.executeQuery();
            for (String colum : getColumns(rs)) {
                rs.first();
                if (rs.getRow() != 0) {
                    String attrname = CommonUtils.getAttribute(colum, attr);
                    if (attrname != null) {
                        map.put(attrname, rs.getString(colum));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeResources(rs, st);
        }
        closeResources(rs, st);
        return map;
    }


    public List<String> getRowData(String table) {
        List<String> list = new ArrayList<>();
        ResultSet rs = null;
        PreparedStatement st = null;
        try {
            st = con.prepareStatement("SELECT * FROM " + table, ResultSet.TYPE_SCROLL_INSENSITIVE, Statement.RETURN_GENERATED_KEYS);
            rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("info"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return list;
        } finally {
            closeResources(rs, st);
        }
        closeResources(rs, st);
        return list;
    }

    public void closeResources(final ResultSet rs, final PreparedStatement st) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection openConnection() {
        if (con == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                String connectionscript = "jdbc:mysql://" + host + ":" + port + "/" + database;
                con = DriverManager.getConnection(connectionscript, user, password);
                return con;
            } catch (Exception e) {
                e.printStackTrace();
                NGXserver.get().getLogger().severe("SQL CONNECTION FAILED");
                con = null;
                return null;
            }
        } else {
            return con;
        }
    }
}
