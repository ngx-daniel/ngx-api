package com.ngxdev.ngxapi.server.scoreboard;

import com.ngxdev.ngxapi.server.events.UpdatedTimeType;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import com.ngxdev.ngxapi.server.scheduler.Scheduler;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.HashMap;

public abstract class ScoreboardCreator {
    private static Scheduler task;
    private static int customtask = -1;
    private Scoreboard score;
    private Objective objective;
    private HashMap<String, Integer> scores;
    private HashMap<String, Integer> refreshScores;
    private String title;
    private boolean titleRefresh;
    private Objective obj;
    private Objective buffer;
    private NGXplayer player;

    public ScoreboardCreator(NGXplayer p) {
        score = Bukkit.getScoreboardManager().getNewScoreboard();
        scores = new HashMap<>();
        player = p;
        refreshScores = new HashMap<>();
        obj = score.getObjective("O-" + MainClass.getMain().getServer().getServerName());
        buffer = score.getObjective("B-" + MainClass.getMain().getServer().getServerName());
        if (obj == null) {
            obj = score.registerNewObjective("O-" + MainClass.getMain().getServer().getServerName(), "dummy");
        }
        if (buffer == null) {
            buffer = score.registerNewObjective("B-" + MainClass.getMain().getServer().getServerName(), "dummy");
        }
        objective = obj;
    }

    public static void startUpdater() {
        if (customtask != -1) MainClass.getMain().getServer().getScheduler().cancelTask(customtask);
        task = new Scheduler() {
            @Override
            public void run() {
                for (NGXplayer np : NGXplayer.getNGXplayers()) {
                    if (np.getScoreboard() != null) {
                        np.updateScoreboard();
                    }
                }
            }
        };
        task.start(UpdatedTimeType.PENTA);
    }

    public static void customUpdater(final int ticks) {
        if (task != null) task.stop();
        customtask = MainClass.getMain().getServer().getScheduler().scheduleSyncRepeatingTask(MainClass.getMain(), new Runnable() {
            @Override
            public void run() {
                for (NGXplayer np : NGXplayer.getNGXplayers()) {
                    if (np.getScoreboard() != null) {
                        np.updateScoreboard();
                    }
                }
            }
        }, 0, ticks);
    }

    public static void setRefreshRate(final UpdatedTimeType speed) {
        ScoreboardCreator.task.setTime(speed);
    }

    public Scoreboard getScoreboard() {
        return score;
    }

    public Objective getObjective() {
        return objective;
    }

    public NGXplayer getPlayer() {
        return player;
    }

    public void setPlayer(NGXplayer player) {
        this.player = player;
    }

    public void setup() {
        createScoreboard();
        obj.setDisplayName(title);
        buffer.setDisplayName(title);
        for (final String s : scores.keySet()) {
            obj.getScore(s).setScore(scores.get(s));
            buffer.getScore(s).setScore(scores.get(s));
        }
    }

    public void create() {
        if (objective == obj) {
            objective = buffer;
        } else {
            objective = obj;
        }
        for (final String s : scores.keySet()) {
            objective.getScore(s).setScore(scores.get(s));
        }
        for (final String entry : refreshScores.keySet()) {
            score.resetScores(entry);
        }
        refreshScores.clear();
        createScoreboard();
        if (titleRefresh) {
            objective.setDisplayName(title);
        }
        for (final String entry : refreshScores.keySet()) {
            objective.getScore(entry).setScore(refreshScores.get(entry));
        }
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void addLine(final String text, final int line) {
        addLine(text, line, false);
    }

    public void addLine(final String raw, final int line, final boolean refresh) {
        String text = CommonUtils.convert(raw);
        if (refresh) {
            refreshScores.put(text, line);
        } else {
            scores.put(text, line);
        }
    }

    public void setTitle(final String title, final boolean refresh) {
        this.title = CommonUtils.convert(title);
        titleRefresh = refresh;
    }

    public void setTitle(final String title) {
        setTitle(title, false);
    }

    public abstract void createScoreboard();
}
