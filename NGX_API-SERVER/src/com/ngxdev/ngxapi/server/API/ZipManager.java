package com.ngxdev.ngxapi.server.API;

import com.ngxdev.ngxapi.server.main.NGXserver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ZipManager {
    private String input,
            destination;

    public ZipManager(String input, String destination) {
        this.input = input;
        this.destination = destination;
    }

    public void unzip() {
        try {
            int BUFFER = 2048;
            File file = new File(input);

            ZipFile zip = new ZipFile(file);
            String newPath = destination;

            new File(newPath).mkdir();
            Enumeration zipFileEntries = zip.entries();

            while (zipFileEntries.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
                String currentEntry = entry.getName();

                File destFile = new File(newPath, currentEntry);
                File destinationParent = destFile.getParentFile();

                destinationParent.mkdirs();

                if (!entry.isDirectory()) {
                    BufferedInputStream is = new BufferedInputStream(zip
                            .getInputStream(entry));
                    int currentByte;
                    byte data[] = new byte[BUFFER];

                    FileOutputStream fos = new FileOutputStream(destFile);
                    BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
                    while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
                        dest.write(data, 0, currentByte);
                    }
                    dest.flush();
                    dest.close();
                    is.close();
                }
            }
        } catch (Exception e) {
            NGXserver.get().info("ERROR: " + e.getMessage());
        }
    }
}
