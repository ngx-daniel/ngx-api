package com.ngxdev.ngxapi.server.API;

import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class DownloadManager {
    private URL url;
    private String destination;

    public DownloadManager(URL url, String destination) {
        this.url = url;
        this.destination = destination;
    }

    public static void updateMe(String url, String name) {
        try {
            download(new URL(url), "plugins/" + name + ".jar");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private static void download(URL url, String destination) {
        try {
            ReadableByteChannel e = Channels.newChannel(url.openStream());
            FileOutputStream fos = new FileOutputStream(destination);
            fos.getChannel().transferFrom(e, 0L, Integer.MAX_VALUE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        download(url, destination);
    }
}
