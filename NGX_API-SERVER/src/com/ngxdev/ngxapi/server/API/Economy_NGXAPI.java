package com.ngxdev.ngxapi.server.API;

import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.player.OfflineNGXplayer;
import net.milkbowl.vault.economy.AbstractEconomy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;

public class Economy_NGXAPI extends AbstractEconomy {
    private static final Logger log = Logger.getLogger("Minecraft");
    private final String name = "NGX_API-SERVER";
    protected MainClass economy = null;
    private Plugin plugin = null;

    public Economy_NGXAPI(Plugin plugin) {
        this.plugin = plugin;
        Bukkit.getServer().getPluginManager().registerEvents(new EconomyServerListener(this), plugin);

        if (economy == null) {
            Plugin ec = plugin.getServer().getPluginManager().getPlugin("NGX_API-SERVER");
            if (ec != null && ec.isEnabled()) {
                economy = (MainClass) ec;
                log.info(String.format("[%s][Economy] %s hooked.", plugin.getDescription().getName(), name));
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isEnabled() {
        return economy != null && economy.isEnabled();
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public int fractionalDigits() {
        return 2;
    }

    public String format(double v) {
        BigDecimal balance = new BigDecimal(v);
        if (balance.compareTo(new BigDecimal(1000000)) >= 0) {
            return balance.setScale(2, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(1000000), BigDecimal.ROUND_HALF_UP).toString() + "M";
        } else if (balance.compareTo(new BigDecimal(1000)) >= 0) {
            return balance.setScale(2, BigDecimal.ROUND_HALF_UP).divide(new BigDecimal(1000), BigDecimal.ROUND_HALF_UP).toString() + "k";
        } else {
            return balance.setScale(2, BigDecimal.ROUND_HALF_UP).toString();
        }
    }

    @Override
    public String currencyNamePlural() {
        return null;
    }

    @Override
    public String currencyNameSingular() {
        return null;
    }

    @Override
    public boolean hasAccount(String s) {
        return false;
    }

    @Override
    public boolean hasAccount(String s, String s1) {
        return false;
    }

    @Override
    public double getBalance(String s) {
        return OfflineNGXplayer.getOfflineNGXplayer(s).eco.getBalance();
    }

    @Override
    public double getBalance(String s, String s1) {
        return OfflineNGXplayer.getOfflineNGXplayer(s).eco.getBalance();
    }

    public void setBalance(String s, double v) {
        OfflineNGXplayer.getOfflineNGXplayer(s).eco.setBalance(v);
    }

    @Override
    public boolean has(String s, double v) {
        return getBalance(s) >= v;
    }

    @Override
    public boolean has(String s, String s1, double v) {
        return getBalance(s) >= v;
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, double v) {
        if (v < 0) {
            return new EconomyResponse(0, getBalance(s), EconomyResponse.ResponseType.FAILURE, "Cannot withdraw negative funds");
        }

        if (!has(s, v)) {
            return new EconomyResponse(0, getBalance(s), EconomyResponse.ResponseType.FAILURE, "Insufficient funds");
        }

        double balance = getBalance(s);
        setBalance(s, balance + v);
        balance = getBalance(s);
        return new EconomyResponse(v, balance, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse withdrawPlayer(String s, String s1, double v) {
        if (v < 0) {
            return new EconomyResponse(0, getBalance(s), EconomyResponse.ResponseType.FAILURE, "Cannot withdraw negative funds");
        }

        if (!has(s, v)) {
            return new EconomyResponse(0, getBalance(s), EconomyResponse.ResponseType.FAILURE, "Insufficient funds");
        }

        double balance = getBalance(s);
        setBalance(s, balance + v);
        balance = getBalance(s);
        return new EconomyResponse(v, balance, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse depositPlayer(String s, double v) {
        if (v < 0) {
            return new EconomyResponse(0, getBalance(s), EconomyResponse.ResponseType.FAILURE, "Cannot deposit negative funds");
        }
        double balance = getBalance(s);
        setBalance(s, balance + v);
        balance = getBalance(s);
        return new EconomyResponse(v, balance, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse depositPlayer(String s, String s1, double v) {
        if (v < 0) {
            return new EconomyResponse(0, getBalance(s), EconomyResponse.ResponseType.FAILURE, "Cannot deposit negative funds");
        }
        double balance = getBalance(s);
        setBalance(s, balance + v);
        balance = getBalance(s);
        return new EconomyResponse(v, balance, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public EconomyResponse createBank(String s, String s1) {
        return null;
    }

    @Override
    public EconomyResponse deleteBank(String s) {
        return null;
    }

    @Override
    public EconomyResponse bankBalance(String s) {
        return null;
    }

    @Override
    public EconomyResponse bankHas(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse bankWithdraw(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse bankDeposit(String s, double v) {
        return null;
    }

    @Override
    public EconomyResponse isBankOwner(String s, String s1) {
        return null;
    }

    @Override
    public EconomyResponse isBankMember(String s, String s1) {
        return null;
    }

    @Override
    public List<String> getBanks() {
        return null;
    }

    @Override
    public boolean createPlayerAccount(String s) {
        return false;
    }

    @Override
    public boolean createPlayerAccount(String s, String s1) {
        return false;
    }

    public class EconomyServerListener implements Listener {
        Economy_NGXAPI economy = null;

        public EconomyServerListener(Economy_NGXAPI economy) {
            this.economy = economy;
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onPluginEnable(PluginEnableEvent event) {
            if (economy.economy == null) {
                Plugin ec = event.getPlugin();

                if (ec.getDescription().getName().equals("NGX_API-SERVER")) {
                    economy.economy = (MainClass) ec;
                    log.info(String.format("[%s][Economy] %s hooked.", plugin.getDescription().getName(), economy.name));
                }
            }
        }

        @EventHandler(priority = EventPriority.MONITOR)
        public void onPluginDisable(PluginDisableEvent event) {
            if (economy.economy != null) {
                if (event.getPlugin().getDescription().getName().equals("NGX_API-SERVER")) {
                    economy.economy = null;
                    log.info(String.format("[%s][Economy] %s unhooked.", plugin.getDescription().getName(), economy.name));
                }
            }
        }
    }
}
