package com.ngxdev.ngxapi.server.API;

import com.ngxdev.ngxapi.server.main.MainClass;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.nio.charset.Charset;

public class ConfigManager {
    private static ConfigManager cfgm;
    MainClass m;

    public ConfigManager() {
        m = MainClass.getMain();
        cfgm = this;
    }

    public static ConfigManager get() {
        return cfgm;
    }

    public Config getNewConfig(String filePath, JavaPlugin p) {

        File file = this.getConfigFile(filePath, p);

        if (!file.exists()) {
            this.prepareFile(filePath, p);
        }

        Config config = new Config(file, this.getCommentsNum(file));
        return config;
    }

    private File getConfigFile(String file, JavaPlugin p) {
        if (file.isEmpty())
            return null;
        File configFile;
        if (file.contains("/"))
            if (file.startsWith("/"))
                configFile = new File(p.getDataFolder() + file.replace("/", File.separator));
            else
                configFile = new File(p.getDataFolder() + File.separator + file.replace("/", File.separator));
        else
            configFile = new File(p.getDataFolder(), file);
        return configFile;
    }

    public void prepareFile(String filePath, JavaPlugin p) {
        File file = this.getConfigFile(filePath, p);
        if (file.exists())
            return;
        try {
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public InputStream getConfigContent(File file) {
        if (!file.exists())
            return null;
        try {
            int commentNum = 0;
            String addLine;
            String currentLine;
            String mName = this.getPluginName();
            StringBuilder whole = new StringBuilder("");
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((currentLine = reader.readLine()) != null)
                if (currentLine.startsWith("#")) {
                    addLine = currentLine.replaceFirst("#", mName + "_COMMENT_" + commentNum + ":");
                    whole.append(addLine + "\n");
                    commentNum++;
                } else
                    whole.append(currentLine + "\n");
            String config = whole.toString();
            InputStream configStream = new ByteArrayInputStream(config.getBytes(Charset.forName("UTF-8")));
            reader.close();
            return configStream;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public InputStream getConfigContent(String filePath, JavaPlugin p) {
        return this.getConfigContent(this.getConfigFile(filePath, p));
    }

    private String prepareConfigString(String configString) {
        int lastLine = 0;
        String[] lines = configString.split("\n");
        StringBuilder config = new StringBuilder("");
        for (String line : lines) {
            if (line.startsWith(this.getPluginName() + "_COMMENT")) {
                String comment = "#" + line.trim().substring(line.indexOf(":") + 1);
                String normalComment;
                if (comment.startsWith("# ' "))
                    normalComment = comment.substring(0, comment.length() - 1).replaceFirst("# ' ", "# ");
                else
                    normalComment = comment;
                if (lastLine == 0) {
                    config.append(normalComment + "\n");
                } else if (lastLine == 1) {
                    config.append("\n" + normalComment + "\n");
                }
                lastLine = 0;
            } else {
                config.append(line + "\n");
                lastLine = 1;
            }
        }
        return config.toString();
    }

    public void saveConfig(String configString, File file) {
        String configuration = this.prepareConfigString(configString);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(configuration);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPluginName() {
        return m.getDescription().getName();
    }

    private void copyResource(InputStream resource, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            int lenght;
            byte[] buf = new byte[1024];
            while ((lenght = resource.read(buf)) > 0)
                out.write(buf, 0, lenght);
            out.close();
            resource.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getCommentsNum(File file) {
        if (!file.exists())
            return 0;
        try {
            int comments = 0;
            String currentLine;
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while ((currentLine = reader.readLine()) != null)
                if (currentLine.startsWith("#"))
                    comments++;
            reader.close();
            return comments;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }

    }
}