package com.ngxdev.ngxapi.server.API;

import java.util.HashMap;

public abstract class ConfigBuilder {
    private Config cfg;
    private int version;
    private HashMap<String, Object> configlist = new HashMap<>();
    private HashMap<String, Object> configlistonce = new HashMap<>();

    public ConfigBuilder(Config cfg, int version) {
        this.cfg = cfg;
        this.version = version;
    }

    public void setOnce(String path, Object obj) {
        configlistonce.put(path, obj);
    }

    public void set(String path, Object obj) {
        configlist.put(path, obj);
    }

    public void build() {
        for (String path : configlist.keySet()) {
            cfg.build(path, configlist.get(path));
        }
        if (!(cfg.contains("version") && cfg.getInt("version") == version)) {
            for (String path : configlistonce.keySet()) {
                cfg.build(path, configlistonce.get(path));
            }
        }
        cfg.build("version", version);
    }
}
