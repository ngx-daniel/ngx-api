package com.ngxdev.ngxapi.server.managers;

import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.main.MainListener;
import com.ngxdev.ngxapi.server.main.NGXserver;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

public class ListenerManager implements Listener {
    public MainClass m;

    public ListenerManager() {
        m = MainClass.getMain();
        NGXserver.get().getPluginManager().registerEvents(this, m);
    }

    public static void setup() {
        new MainListener();
    }

    public static Listener register(Listener l) {
        NGXserver server = NGXserver.get();
        unregister(l);
        server.getPluginManager().registerEvents(l, MainClass.getMain());
        return l;
    }

    public static void unregister(Listener l) {
        HandlerList.unregisterAll(l);
    }
}
