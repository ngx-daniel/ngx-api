package com.ngxdev.ngxapi.server.managers;


import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class NGXinventory implements Listener {
    private String name;
    private Player player;
    private int size;
    private OptionClickEventHandler handler;
    private ItemStack[] optionIcons;

    public NGXinventory(String name, int size, OptionClickEventHandler handler) {
        this.name = CommonUtils.convert(name);
        this.size = size;
        this.handler = handler;
        this.optionIcons = new ItemStack[size];
    }

    public NGXinventory setOption(int position, ItemStack icon) {
        optionIcons[position] = icon;
        return this;
    }

    public void open(Player player) {
        ListenerManager.register(this);
        this.player = player;
        Inventory inventory = Bukkit.createInventory(player, size, name);
        for (int i = 0; i < optionIcons.length; i++) {
            if (optionIcons[i] != null) {
                inventory.setItem(i, optionIcons[i]);
            }
        }
        player.openInventory(inventory);
    }

    public void destroy() {
        ListenerManager.unregister(this);
        handler = null;
        optionIcons = null;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getInventory().getName().equals(name)) {
            e.setCancelled(true);
            int slot = e.getRawSlot();
            if (slot >= 0 && slot < size && optionIcons[slot] != null) {
                OptionClickEvent ev = new OptionClickEvent((Player) e.getWhoClicked(), slot, optionIcons[slot]);
                handler.onOptionClick(ev);
                if (ev.willDestroy()) {
                    destroy();
                }
            }
        }
    }

    public Player getPlayer() {
        return player;
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (e.getInventory().getName().equals(name)) {
            destroy();
        }
    }

    public interface OptionClickEventHandler {
        void onOptionClick(OptionClickEvent e);
    }

    public class OptionClickEvent {
        private Player player;
        private int position;
        private ItemStack icon;
        private boolean destroy;

        public OptionClickEvent(Player player, int position, ItemStack icon) {
            this.player = player;
            this.position = position;
            this.icon = icon;
            this.destroy = false;
        }

        public Player getPlayer() {
            return player;
        }

        public int getPosition() {
            return position;
        }

        public ItemStack getIcon() {
            return icon;
        }

        public boolean willDestroy() {
            return destroy;
        }

        public void setWillDestroy(boolean destroy) {
            this.destroy = destroy;
        }
    }
}
