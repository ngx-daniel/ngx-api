package com.ngxdev.ngxapi.server.managers;

import com.ngxdev.ngxapi.server.interfaces.NGXicommand;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.main.NGXserver;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import com.ngxdev.ngxapi.server.utils.CmdMap;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

import java.util.HashMap;

public abstract class NGXcommand implements NGXicommand, CommandExecutor {
    private static HashMap<String, NGXicommand> commands = new HashMap<>();
    public MainClass m;
    public NGXserver server;
    public NGXplayer p;
    public String name;
    public String usage;
    public String permission;
    public String desc;
    public String[] args;
    private boolean canConsole = false;
    private String[] cmds;
    private CommandSender s;

    public NGXcommand(String description, String usage, boolean canConsole, String permission, String... cmds) {
        m = MainClass.getMain();
        server = NGXserver.get();

        this.usage = usage;
        this.permission = permission;
        this.canConsole = canConsole;
        this.desc = description;
        this.cmds = cmds;

        register();
    }

    public static void unregisterCommand(String main) {
        MainClass m = MainClass.getMain();
        PluginCommand command = CmdMap.getCommand(main, m);
        command.getAliases().clear();
        command.unregister(CmdMap.getCommandMap());
    }

    public static void unregisterCommands() {
        for (String cmd : commands.keySet()) {
            unregisterCommand(cmd);
        }
        commands.clear();
    }

    private void register() {
        for (String cmd : cmds) {
            CmdMap.unregister(cmd);
            PluginCommand command = CmdMap.getCommand(cmd, m);
            command.setDescription(desc);
            command.setUsage(usage);
            command.setPermission(permission);
            CmdMap.getCommandMap().register("NGXAPI", command);
            server.getPluginCommand(cmd).setExecutor(this);
            commands.put(cmd, this);
        }
    }

    public void sendMessage(String msg) {
        s.sendMessage(CommonUtils.convert(msg));
    }

    public void notenoughargs() {
        sendMessage("&4Error&f: &cNot enough arguments. Usage: \"" + usage + "\"");
    }

    public void toomanyargs() {
        sendMessage("&4Error&f: &cToo many arguments. Usage: \"" + usage + "\"");
    }

    public void invalidargs() {
        sendMessage("&4Error&f: &cInvalid arguments. Usage: \"" + usage + "\"");
    }

    public void error(String msg) {
        sendMessage("&4Error&f: &c" + msg);
    }

    @Override
    public boolean onCommand(CommandSender s, Command cmd, String label, String[] args) {
        this.s = s;
        this.args = args;
        this.name = cmd.getName();
        if (s instanceof Player) {
            p = NGXplayer.getNGXplayer((Player) s);
            commands.get(cmd.getName()).exec();
        } else {
            if (canConsole) commands.get(cmd.getName()).exec();
            else sendMessage("Command " + cmd.getName() + " cannot be executed inside the console.");
        }
        return true;
    }
}
