package com.ngxdev.ngxapi.server.main;

import com.ngxdev.ngxapi.server.events.ServerPredisableEvent;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import com.ngxdev.ngxapi.server.managers.NGXcommand;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import com.ngxdev.ngxapi.server.utils.SocketUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

public class MainListener implements Listener {
    public MainListener() {
        ListenerManager.register(this);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void preDisable(PlayerCommandPreprocessEvent e) {
        if (e.getMessage().startsWith("/stop")) {
            e.setCancelled(true);
            ServerPredisableEvent event = new ServerPredisableEvent();
            Bukkit.getServer().getPluginManager().callEvent(event);
            MainClass.getMain().getServer().getScheduler().scheduleSyncDelayedTask(MainClass.getMain(), new Runnable() {
                @Override
                public void run() {
                    Bukkit.shutdown();
                }
            }, 20);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void Join(PlayerJoinEvent e) {
        if (SocketUtils.active) SocketUtils.sendData("servers", "update", "bungee");
        Player p = e.getPlayer();
        NGXplayer np = NGXplayer.createPlayer(p);
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void Kick(PlayerKickEvent e) {
        if (SocketUtils.active) SocketUtils.sendData("servers", "update", "bungee");
        NGXplayer np = NGXplayer.getNGXplayer(e.getPlayer());
        np.remove();
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void Quit(PlayerQuitEvent e) {
        if (SocketUtils.active) SocketUtils.sendData("servers", "update", "bungee");
        NGXplayer np = NGXplayer.getNGXplayer(e.getPlayer());
        np.remove();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onReload(PlayerCommandPreprocessEvent e) {
        if (e.getMessage().startsWith("/reload")) {
            NGXcommand.unregisterCommands();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void cmdpaper(PlayerInteractEvent e) {
        MainClass m = MainClass.getMain();
        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Player p = e.getPlayer();
            ItemStack hand = p.getItemInHand();
            if (hand.hasItemMeta() && hand.getItemMeta().getDisplayName() != null) {
                if (hand.getItemMeta().getDisplayName().startsWith(CommonUtils.convert("&a|CMD|"))) {
                    m.getServer().dispatchCommand(m.getServer().getConsoleSender(), CommonUtils.strip(hand.getItemMeta().getLore().get(0)).replace("%player%", p.getName()).replace("/", ""));
                    if (hand.getAmount() > 1) {
                        hand.setAmount(hand.getAmount() - 1);
                    } else {
                        p.getInventory().remove(hand);
                    }
                }
            }
        }
    }

    @EventHandler
    public void frozen(final PlayerMoveEvent e) {
        final Player p = e.getPlayer();
        final Location loc = e.getFrom();
        final Location loc2 = e.getTo();
        if (NGXplayer.frozen.contains(NGXplayer.getNGXplayer(p)) && (loc2.getX() != loc.getX() || loc2.getZ() != loc.getZ())) {
            e.getPlayer().teleport(loc);
        }
    }
}
