package com.ngxdev.ngxapi.server.main;

import com.avaje.ebean.config.ServerConfig;
import com.ngxdev.ngxapi.server.player.NGXplayer;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.*;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.help.HelpMap;
import org.bukkit.inventory.*;
import org.bukkit.map.MapView;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.util.CachedServerIcon;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.*;
import java.util.logging.Logger;

public class NGXserver {
    private static NGXserver ngxserver;
    private MainClass m;
    private Server server;

    public NGXserver() {
        m = MainClass.getMain();
        server = m.getServer();
        ngxserver = this;
    }

    public static NGXserver get() {
        return ngxserver;
    }

    public String getRootFolder() {
        return m.getServer().getWorldContainer().getAbsolutePath();
    }

    public void info(String msg) {
        server.getLogger().info(CommonUtils.convert(msg));
    }

    public void warn(String msg) {
        server.getLogger().warning(CommonUtils.convert(msg));
    }

    public void error(String msg) {
        server.getLogger().severe(CommonUtils.convert(msg));
    }

    public void debug(String msg) {
        for (NGXplayer p : NGXplayer.getNGXplayers()) {
            if (p.debugMode()) p.sendMessage("&aDEBUG: &f" + msg);
        }
    }

    public void broadcast(String msg) {
        for (NGXplayer p : NGXplayer.getNGXplayers()) {
            p.sendMessage(msg);
        }
    }

    public String getName() {
        return server.getName();
    }

    public String getVersion() {
        return server.getVersion();
    }

    public String getBukkitVersion() {
        return server.getBukkitVersion();
    }

    public Collection<? extends Player> getOnlinePlayers() {
        return server.getOnlinePlayers();
    }

    public int getMaxPlayers() {
        return server.getMaxPlayers();
    }

    public int getPort() {
        return server.getPort();
    }

    public int getViewDistance() {
        return server.getViewDistance();
    }

    public String getIp() {
        return server.getIp();
    }

    public String getServerName() {
        return server.getServerName();
    }

    public String getServerId() {
        return server.getServerId();
    }

    public String getWorldType() {
        return server.getWorldType();
    }

    public boolean getGenerateStructures() {
        return server.getGenerateStructures();
    }

    public boolean getAllowNether() {
        return server.getAllowNether();
    }

    public boolean hasWhitelist() {
        return server.hasWhitelist();
    }

    public int broadcastMessage(String message) {
        return server.broadcastMessage(message);
    }

    public String getUpdateFolder() {
        return server.getUpdateFolder();
    }

    public Player getPlayer(UUID id) {
        return server.getPlayer(id);
    }

    public PluginManager getPluginManager() {
        return server.getPluginManager();
    }

    public BukkitScheduler getScheduler() {
        return server.getScheduler();
    }

    public ServicesManager getServicesManager() {
        return server.getServicesManager();
    }

    public List<World> getWorlds() {
        return server.getWorlds();
    }

    public World createWorld(WorldCreator options) {
        return server.createWorld(options);
    }

    public boolean unloadWorld(String name, boolean save) {
        return server.unloadWorld(name, save);
    }

    public boolean unloadWorld(World world, boolean save) {
        return server.unloadWorld(world, save);
    }

    public World getWorld(String name) {
        return server.getWorld(name);
    }

    public World getWorld(UUID uid) {
        return server.getWorld(uid);
    }

    @Deprecated
    public MapView getMap(short id) {
        return server.getMap(id);
    }

    public MapView createMap(World world) {
        return server.createMap(world);
    }

    public void reload() {
        server.reload();
    }

    public Logger getLogger() {
        return server.getLogger();
    }

    public PluginCommand getPluginCommand(String name) {
        return server.getPluginCommand(name);
    }

    public void savePlayers() {
        server.savePlayers();
    }

    public boolean dispatchCommand(CommandSender sender, String commandLine) throws CommandException {
        return server.dispatchCommand(sender, commandLine);
    }

    public void configureDbConfig(ServerConfig config) {
        server.configureDbConfig(config);
    }

    public boolean addRecipe(Recipe recipe) {
        return server.addRecipe(recipe);
    }

    public List<Recipe> getRecipesFor(ItemStack result) {
        return server.getRecipesFor(result);
    }

    public Iterator<Recipe> recipeIterator() {
        return server.recipeIterator();
    }

    public void clearRecipes() {
        server.clearRecipes();
    }

    public void resetRecipes() {
        server.resetRecipes();
    }

    public Map<String, String[]> getCommandAliases() {
        return server.getCommandAliases();
    }

    public int getSpawnRadius() {
        return server.getSpawnRadius();
    }

    public void setSpawnRadius(int value) {
        server.setSpawnRadius(value);
    }

    public boolean getOnlineMode() {
        return server.getOnlineMode();
    }

    public boolean getAllowFlight() {
        return server.getAllowFlight();
    }

    public boolean isHardcore() {
        return server.isHardcore();
    }

    public void shutdown() {
        server.shutdown();
    }

    public int broadcast(String message, String permission) {
        return server.broadcast(message, permission);
    }

    public OfflinePlayer getOfflinePlayer(UUID id) {
        return server.getOfflinePlayer(id);
    }

    public Set<String> getIPBans() {
        return server.getIPBans();
    }

    public void banIP(String address) {
        server.banIP(address);
    }

    public void unbanIP(String address) {
        server.unbanIP(address);
    }

    public Set<OfflinePlayer> getBannedPlayers() {
        return server.getBannedPlayers();
    }

    public BanList getBanList(BanList.Type type) {
        return server.getBanList(type);
    }

    public void setWhitelist(boolean value) {
        server.setWhitelist(value);
    }

    public Set<OfflinePlayer> getWhitelistedPlayers() {
        return server.getWhitelistedPlayers();
    }

    public void reloadWhitelist() {
        server.reloadWhitelist();
    }

    public ConsoleCommandSender getConsoleSender() {
        return server.getConsoleSender();
    }

    public Set<OfflinePlayer> getOperators() {
        return server.getOperators();
    }

    public File getWorldContainer() {
        return server.getWorldContainer();
    }

    public Messenger getMessenger() {
        return server.getMessenger();
    }

    public boolean getAllowEnd() {
        return server.getAllowEnd();
    }

    public File getUpdateFolderFile() {
        return server.getUpdateFolderFile();
    }

    public long getConnectionThrottle() {
        return server.getConnectionThrottle();
    }

    public int getTicksPerAnimalSpawns() {
        return server.getTicksPerAnimalSpawns();
    }

    public int getTicksPerMonsterSpawns() {
        return server.getTicksPerMonsterSpawns();
    }

    public GameMode getDefaultGameMode() {
        return server.getDefaultGameMode();
    }

    public void setDefaultGameMode(GameMode mode) {
        server.setDefaultGameMode(mode);
    }

    public OfflinePlayer[] getOfflinePlayers() {
        return server.getOfflinePlayers();
    }

    public Inventory createInventory(InventoryHolder owner, InventoryType type) {
        return server.createInventory(owner, type);
    }

    public Inventory createInventory(InventoryHolder owner, InventoryType type, String title) {
        return server.createInventory(owner, type, title);
    }

    public Inventory createInventory(InventoryHolder owner, int size) throws IllegalArgumentException {
        return server.createInventory(owner, size);
    }

    public Inventory createInventory(InventoryHolder owner, int size, String title) throws IllegalArgumentException {
        return server.createInventory(owner, size, title);
    }

    public HelpMap getHelpMap() {
        return server.getHelpMap();
    }

    public int getMonsterSpawnLimit() {
        return server.getMonsterSpawnLimit();
    }

    public int getAnimalSpawnLimit() {
        return server.getAnimalSpawnLimit();
    }

    public int getWaterAnimalSpawnLimit() {
        return server.getWaterAnimalSpawnLimit();
    }

    public int getAmbientSpawnLimit() {
        return server.getAmbientSpawnLimit();
    }

    public boolean isPrimaryThread() {
        return server.isPrimaryThread();
    }

    public String getMotd() {
        return server.getMotd();
    }

    public String getShutdownMessage() {
        return server.getShutdownMessage();
    }

    public Warning.WarningState getWarningState() {
        return server.getWarningState();
    }

    public ItemFactory getItemFactory() {
        return server.getItemFactory();
    }

    public ScoreboardManager getScoreboardManager() {
        return server.getScoreboardManager();
    }

    public CachedServerIcon getServerIcon() {
        return server.getServerIcon();
    }

    public CachedServerIcon loadServerIcon(File file) throws Exception {
        return server.loadServerIcon(file);
    }

    public CachedServerIcon loadServerIcon(BufferedImage image) throws Exception {
        return server.loadServerIcon(image);
    }

    public int getIdleTimeout() {
        return server.getIdleTimeout();
    }

    public void setIdleTimeout(int threshold) {
        server.setIdleTimeout(threshold);
    }
}
