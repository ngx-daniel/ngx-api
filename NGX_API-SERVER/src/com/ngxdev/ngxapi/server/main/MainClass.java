package com.ngxdev.ngxapi.server.main;

import com.ngxdev.ngxapi.server.API.Config;
import com.ngxdev.ngxapi.server.API.ConfigManager;
import com.ngxdev.ngxapi.server.API.DownloadManager;
import com.ngxdev.ngxapi.server.API.Economy_NGXAPI;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import com.ngxdev.ngxapi.server.scheduler.Scheduler;
import com.ngxdev.ngxapi.server.scoreboard.ScoreboardCreator;
import com.ngxdev.ngxapi.server.utils.SocketUtils;
import com.ngxdev.ngxapi.server.utils.TickUtils;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;


public class MainClass extends JavaPlugin {
    private static MainClass m;
    public Config config,
            gameconfig,
            data;

    public static MainClass getMain() {
        return m;
    }

    public void onLoad() {
        m = this;
        setupConfig();
        new NGXserver();
        hookEconomy();
    }

    public void onEnable() {
        setup();
        checkBungee();
    }

    public void onDisable() {
        if (SocketUtils.active) SocketUtils.sendData("servers", "update", "bungee");
        DownloadManager.updateMe("https://www.dropbox.com/s/jbl1t03iw5pqqyr/NGX_API-SERVER.jar?dl=1", "NGX_API-SERVER");
        SocketUtils.close();
        m = null;
    }

    private void checkBungee() {
        m.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    private void setup() {
        ListenerManager.setup();
        Scheduler.setup();
        ScoreboardCreator.startUpdater();
        TickUtils.setup();
    }

    private void setupConfig() {
        ConfigManager cfgm = new ConfigManager();
        config = cfgm.getNewConfig("config.yml", m);
        gameconfig = cfgm.getNewConfig("gameconfig.yml", m);
        data = cfgm.getNewConfig("data.yml", m);
    }

    private void hookEconomy() {
        try {
            Economy econ = Economy_NGXAPI.class.getConstructor(Plugin.class).newInstance(this);
            getServer().getServicesManager().register(Economy.class, econ, this, ServicePriority.Highest);
            NGXserver.get().info(String.format("[Economy] %s found: %s", "NGX_API-ECONOMY", econ.isEnabled() ? "Loaded" : "Waiting"));
        } catch (Exception e) {
            NGXserver.get().error(String.format("[Economy] There was an error hooking %s - check to make sure you're using a compatible version!", "NGX_API-ECONOMY"));
        }
    }
}
