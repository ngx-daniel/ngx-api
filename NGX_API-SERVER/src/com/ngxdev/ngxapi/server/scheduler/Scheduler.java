package com.ngxdev.ngxapi.server.scheduler;

import com.ngxdev.ngxapi.server.events.UpdateEvent;
import com.ngxdev.ngxapi.server.events.UpdatedTimeType;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;

public abstract class Scheduler {
    private static ArrayList<Scheduler> tasks = new ArrayList<>();

    private int taskID;
    private UpdatedTimeType time;

    public Scheduler() {
        taskID = 0;
    }

    public static void setup() {
        MainClass.getMain().getServer().getScheduler().scheduleSyncRepeatingTask(MainClass.getMain(), new Scheduler.Updater(), 0, 1);
        ListenerManager.register(new Scheduler.Updater());
    }

    public static ArrayList<Scheduler> getTasks() {
        return tasks;
    }

    public static Scheduler getScheduler(final int id) {
        for (final Scheduler s : getTasks()) {
            if (s.getTaskID() == id) {
                return s;
            }
        }
        return null;
    }

    private static boolean checkID(final int id) {
        for (final Scheduler s : getTasks()) {
            if (s.getTaskID() == id) {
                return true;
            }
        }
        return false;
    }

    private int getNewID() {
        int id;
        for (id = 1; checkID(id); ++id) {
        }
        return id;
    }

    public int start(UpdatedTimeType time) {
        taskID = getNewID();
        this.time = time;
        Scheduler.tasks.add(this);
        return taskID;
    }

    public void stop() {
        Scheduler.tasks.remove(this);
    }

    public abstract void run();

    public int getTaskID() {
        return taskID;
    }

    public void setTaskID(final int taskID) {
        this.taskID = taskID;
    }

    public UpdatedTimeType getTime() {
        return time;
    }

    public void setTime(UpdatedTimeType time) {
        this.time = time;
    }

    static class Updater implements Listener, Runnable {
        @EventHandler
        public void onUpdate(UpdateEvent e) {
            for (final Scheduler s : Scheduler.getTasks()) {
                if (s.getTime() == e.getType()) {
                    s.run();
                }
            }
        }

        @Override
        public void run() {
            for (UpdatedTimeType type : UpdatedTimeType.values()) {
                if (type.passed()) {
                    MainClass.getMain().getServer().getPluginManager().callEvent(new UpdateEvent(type));
                }
            }
        }
    }
}
