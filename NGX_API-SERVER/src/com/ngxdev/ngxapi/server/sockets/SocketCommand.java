package com.ngxdev.ngxapi.server.sockets;

import com.ngxdev.ngxapi.server.managers.NGXcommand;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import com.ngxdev.ngxapi.server.utils.SocketUtils;

public class SocketCommand extends NGXcommand {
    public SocketCommand() {
        super("Send a socket message.", "/sendsocket <channel> <message>", true, "ngx.sendsocket", "sendsocket", "socket", "socketmessage", "ss");
    }

    @Override
    public void exec() {
        switch (args.length) {
            case 0:
                notenoughargs();
                break;
            case 1:
                notenoughargs();
                break;
            case 2:
                SocketUtils.sendData(args[1], args[0], "all");
                break;
            case 3:
                p.sendMessage("Sending socket message to all servers. Channel: " + args[0] + " Message: " + CommonUtils.getArgs(args, 2));
                SocketUtils.sendData(CommonUtils.getArgs(args, 1), args[0], "all");
                break;
        }
    }
}
