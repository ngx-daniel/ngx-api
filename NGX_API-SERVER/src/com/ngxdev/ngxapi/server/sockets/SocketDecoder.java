package com.ngxdev.ngxapi.server.sockets;


import com.ngxdev.ngxapi.server.events.PluginMessageReceiveEvent;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.managers.ListenerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SocketDecoder implements Listener {
    private MainClass m;

    public SocketDecoder() {
        m = MainClass.getMain();
        new SocketCommand();
        ListenerManager.register(this);
    }

    @EventHandler
    public void decode(PluginMessageReceiveEvent e) {
        String channel = e.getChannel();
        String message = e.getMessage();
        if (channel.equalsIgnoreCase("command")) {
            m.getServer().dispatchCommand(m.getServer().getConsoleSender(), message);
        }
    }
}
