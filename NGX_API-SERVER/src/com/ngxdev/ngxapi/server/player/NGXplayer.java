package com.ngxdev.ngxapi.server.player;

import com.ngxdev.ngxapi.server.API.Config;
import com.ngxdev.ngxapi.server.API.ConfigManager;
import com.ngxdev.ngxapi.server.database.SQL;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.scoreboard.ScoreboardCreator;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import java.util.ArrayList;
import java.util.List;

public class NGXplayer {
    public static ArrayList<NGXplayer> frozen = new ArrayList<>();
    private static ArrayList<NGXplayer> ngxplayers = new ArrayList<>();
    public PacketPlayer packet;
    public NGXeconomy eco;
    private Player p;
    private SQL database;
    private Config playerdata;
    private ScoreboardCreator scoreboard;

    private NGXplayer(final Player p) {
        this.p = p;
        ngxplayers.add(this);
        playerdata = new ConfigManager().getNewConfig("playerdata/" + p.getUniqueId() + ".yml", MainClass.getMain());
        if (playerdata.get("firstjoin") == null) setData("firstjoin", true);
        else setData("firstjoin", false);
        if (SQL.getInstance() != null) {
            database = SQL.getInstance();
            database.openConnection();
            createPlayerData();
        }
        eco = new NGXeconomy(this);
        String name = Bukkit.getServer().getClass().getPackage().getName();
        String version = name.substring(name.lastIndexOf('.') + 1);

        switch (version) {
            case "v1_8_R3":
                packet = new PlayerPacketv1_8_R3(this);
                break;
            case "v1_8_R2":
                packet = new PlayerPacketv1_8_R2(this);
                break;
            case "v1_8_R1":
                packet = new PlayerPacketv1_8_R1(this);
                break;
            default:
                packet = new PlayerPacketDynamic();
                break;
        }
        playerdata.reloadConfig();
    }

    public static NGXplayer createPlayer(Player p) {
        return new NGXplayer(p);
    }

    public static NGXplayer getNGXplayer(Player p) {
        for (NGXplayer ngxp : ngxplayers) {
            if (ngxp.getPlayer() == p) {
                return ngxp;
            }
        }
        return null;
    }

    public static NGXplayer getNGXplayer(String s) {
        for (NGXplayer ngxp : ngxplayers) {
            if (ngxp.getName().equals(s)) {
                return ngxp;
            }
        }
        return null;
    }

    public static NGXplayer getNGXplayerFromUUID(String s) {
        for (NGXplayer ngxp : ngxplayers) {
            if (ngxp.getUUID().equals(s)) {
                return ngxp;
            }
        }
        return null;
    }

    public static ArrayList<NGXplayer> getNGXplayers() {
        return ngxplayers;
    }

    public void reloadConfig() {
        playerdata.reloadConfig();
    }

    public OfflineNGXplayer getOfflinePlayer() {
        return OfflineNGXplayer.getOfflineNGXplayer(p.getUniqueId().toString());
    }

    public void freeze(boolean freeze) {
        if (freeze) {
            if (frozen.contains(this)) return;
            frozen.add(this);
        } else {
            frozen.remove(this);
        }
    }

    private void createPlayerData() {
        if (!database.checkExist(database.getDatabase() + "_data", getUUID())) {
            database.openConnection();
            database.sendUpdateQuery("INSERT INTO " + database.getDatabase() + "_data (info) VALUES ( '" + getUUID() + "')");
        }
    }

    public boolean checkExistance() {
        return getBoolean("firstjoin");
    }

    public boolean isFirstJoin() {
        return getBoolean("firstjoin");
    }

    public void remove() {
        ngxplayers.remove(this);
    }

    public void sendMessage(String s) {
        p.sendMessage(CommonUtils.convert(s));
    }

    public Player getPlayer() {
        return p;
    }

    public String getName() {
        return p.getName();
    }

    public String getUUID() {
        return p.getUniqueId() + "";
    }

    public boolean debugMode() {
        return p.isOp();
    }

    public void createGlobalData(String name, String value) {
        database.openConnection();
        database.createData(database.getDatabase() + "_data", getUUID(), name, value);
    }

    public void setGlobalData(String name, String value) {
        database.openConnection();
        database.setData(database.getDatabase() + "_data", getUUID(), name, value);
    }

    public String getGlobalData(String name) {
        database.openConnection();
        return database.getData(database.getDatabase() + "_data", getUUID(), name);
    }

    public void setData(String path, Object value) {
        playerdata.set(path, value);
    }

    public void createData(String path, Object value) {
        if (playerdata.get(path) == null)
            playerdata.set(path, value);
    }

    public int getInt(String path) {
        return playerdata.getInt(path);
    }

    public double getDouble(String path) {
        return playerdata.getDouble(path);
    }

    public long getLong(String path) {
        return playerdata.getLong(path);
    }

    public boolean getBoolean(String path) {
        return playerdata.getBoolean(path);
    }

    public String getString(String path) {
        return playerdata.getString(path);
    }

    public List<String> getStringList(String path) {
        return playerdata.getStringList(path);
    }

    public ConfigurationSection getConfigurationSection(String path) {
        return playerdata.getConfigurationSection(path);
    }

    public void removeData(String path) {
        playerdata.removeKey(path);
    }

    public void clearScoreboard() {
        scoreboard = null;
    }

    public void updateScoreboard() {
        scoreboard.create();
        getPlayer().setScoreboard(scoreboard.getScoreboard());
    }

    public ScoreboardCreator getScoreboard() {
        return scoreboard;
    }

    public void setScoreboard(ScoreboardCreator sc) {
        Scoreboard sb = sc.getScoreboard();
        for (String entry : sb.getEntries()) {
            sb.resetScores(entry);
        }
        clearScoreboard();
        sc.setup();
        scoreboard = sc;
        getPlayer().setScoreboard(scoreboard.getScoreboard());
    }
}
