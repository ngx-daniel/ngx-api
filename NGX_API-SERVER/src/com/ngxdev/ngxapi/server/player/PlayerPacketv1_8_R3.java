package com.ngxdev.ngxapi.server.player;

import com.ngxdev.ngxapi.server.NMS.FakeWither;
import com.ngxdev.ngxapi.server.main.MainClass;
import com.ngxdev.ngxapi.server.utils.CommonUtils;
import com.ngxdev.ngxapi.server.utils.PacketUtils;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.UUID;

public class PlayerPacketv1_8_R3 extends PacketPlayer {
    private static HashMap<UUID, FakeWither> players = new HashMap<>();
    private static HashMap<UUID, Integer> timers = new HashMap<>();
    NGXplayer p;

    public PlayerPacketv1_8_R3(NGXplayer p) {
        this.p = p;
    }

    public static boolean hasBar(Player player) {
        return players.get(player.getUniqueId()) != null;
    }

    public static void removeBar(Player player) {
        if (getWither(player) != null) {
            PacketUtils.sendPacket(player, getWither(player).getDestroyPacket());
            players.remove(player.getUniqueId());
            cancelTimer(player);
        }
    }

    private static String cleanMessage(String message) {
        if (message.length() > 64)
            message = message.substring(0, 63);

        return CommonUtils.convert(message);
    }

    private static void cancelTimer(Player player) {
        Integer timerID = timers.get(player.getUniqueId());
        if (timerID != null) {
            Bukkit.getScheduler().cancelTask(timerID);
        }
        timers.remove(player);
    }

    private static void sendWither(FakeWither wither, Player player) {
        PacketUtils.sendPacket(player, wither.getMetaPacket(wither.getWatcher()));
        PacketUtils.sendPacket(player, wither.getTeleportPacket(CommonUtils.getfromloc(player.getLocation(), 19)));
    }

    private static FakeWither getWither(Player p) {
        return players.get(p);
    }

    private static FakeWither addWither(Player player, String message) {
        removeBar(player);
        FakeWither wither = PacketUtils.newWither(message, CommonUtils.getfromloc(player.getLocation(), 19));
        PacketUtils.sendPacket(player, wither.getSpawnPacket());
        players.put(player.getUniqueId(), wither);
        return wither;
    }

    public void sendTitle(Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle) {
        PlayerConnection connection = ((CraftPlayer) p.getPlayer()).getHandle().playerConnection;

        PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn, stay, fadeOut);
        connection.sendPacket(packetPlayOutTimes);

        subtitle = CommonUtils.convert(subtitle);
        IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{'text': '" + subtitle + "'}");
        PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
        connection.sendPacket(packetPlayOutSubTitle);

        title = CommonUtils.convert(title);
        IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{'text': '" + title + "'}");
        PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
        connection.sendPacket(packetPlayOutTitle);
    }

    public void sendTabTitle(String header, String footer) {
        if (header == null) header = "";
        header = CommonUtils.convert(header);

        if (footer == null) footer = "";
        footer = CommonUtils.convert(footer);

        PlayerConnection connection = ((CraftPlayer) p.getPlayer()).getHandle().playerConnection;
        IChatBaseComponent tabTitle = IChatBaseComponent.ChatSerializer.a("{'text': '" + header + "'}");
        IChatBaseComponent tabFoot = IChatBaseComponent.ChatSerializer.a("{'text': '" + footer + "'}");
        PacketPlayOutPlayerListHeaderFooter headerPacket = new PacketPlayOutPlayerListHeaderFooter(tabTitle);

        try {
            Field field = headerPacket.getClass().getDeclaredField("b");
            field.setAccessible(true);
            field.set(headerPacket, tabFoot);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.sendPacket(headerPacket);
        }
    }

    public void sendActionBar(String message) {
        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{'text': '" + CommonUtils.convert(message) + "'}");
        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
        ((CraftPlayer) p.getPlayer()).getHandle().playerConnection.sendPacket(ppoc);
    }

    public void sendBossMessage(final String message, final int percent) {
        removeBar(p.getPlayer());
        final FakeWither wither = addWither(p.getPlayer(), cleanMessage(message));
        wither.setHealth(percent);
        timers.put(p.getPlayer().getUniqueId(), Bukkit.getScheduler().runTaskTimer(MainClass.getMain(), new Runnable() {
            @Override
            public void run() {
                sendWither(wither, p.getPlayer());
            }
        }, 0, 2).getTaskId());
        sendWither(wither, p.getPlayer());
    }
}
