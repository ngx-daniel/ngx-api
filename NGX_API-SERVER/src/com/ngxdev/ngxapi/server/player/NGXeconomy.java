package com.ngxdev.ngxapi.server.player;

public class NGXeconomy {
    OfflineNGXplayer op;

    public NGXeconomy(NGXplayer p) {
        this.op = p.getOfflinePlayer();
        op.createData("balance", 0);
    }

    public NGXeconomy(OfflineNGXplayer p) {
        this.op = p;
        op.createData("balance", 0);
    }

    public double getBalance() {
        return op.getDouble("balance");
    }

    public void setBalance(double value) {
        op.setData("balance", value);
    }

    public void addBalance(double value) {
        op.setData("balance", getBalance() + value);
    }

    public void removeBalance(double value) {
        op.setData("balance", getBalance() - value);
    }
}
