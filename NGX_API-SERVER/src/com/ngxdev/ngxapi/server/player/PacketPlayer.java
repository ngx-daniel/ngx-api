package com.ngxdev.ngxapi.server.player;

public abstract class PacketPlayer {
    public abstract void sendTitle(Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle);

    public abstract void sendTabTitle(String header, String footer);

    public abstract void sendActionBar(String message);

    public abstract void sendBossMessage(final String message, final int percent);
}
