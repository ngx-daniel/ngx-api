package com.ngxdev.ngxapi.server.player;

import com.ngxdev.ngxapi.server.API.Config;
import com.ngxdev.ngxapi.server.API.ConfigManager;
import com.ngxdev.ngxapi.server.database.SQL;
import com.ngxdev.ngxapi.server.main.MainClass;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class OfflineNGXplayer {
    public NGXeconomy eco;
    private boolean inChase;
    private String uuid;
    private Config playerdata;
    private SQL database;

    private OfflineNGXplayer(String uuid) {
        if (!inChase) {
            inChase = true;
            this.uuid = uuid;
            playerdata = new ConfigManager().getNewConfig("playerdata/" + uuid + ".yml", MainClass.getMain());
            eco = new NGXeconomy(this);
            if (SQL.getInstance() != null) {
                database = SQL.getInstance();
            }
        }
    }

    public static List<OfflineNGXplayer> getOfflinePlayers() {
        List<OfflineNGXplayer> onps = new ArrayList<>();
        File folder = new File(MainClass.getMain().getDataFolder() + "/playerdata");
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                onps.add(getOfflineNGXplayer(listOfFiles[i].getName().replace(".yml", "")));
            }
        }
        return onps;
    }

    public static OfflineNGXplayer getOfflineNGXplayer(String uuid) {
        return new OfflineNGXplayer(uuid);
    }

    public String getUUID() {
        return uuid;
    }

    public void setGlobalData(String name, String value) {
        database.openConnection();
        database.setData(database.getDatabase() + "_data", uuid, name, value);
    }

    public String getGlobalData(String name) {
        database.openConnection();
        return database.getData(database.getDatabase() + "_data", uuid, name);
    }

    public void setData(String path, Object value) {
        playerdata.set(path, value);
    }

    public void createData(String path, Object value) {
        if (playerdata.get(path) == null)
            playerdata.set(path, value);
    }

    public int getInt(String path) {
        return playerdata.getInt(path);
    }

    public double getDouble(String path) {
        return playerdata.getDouble(path);
    }

    public long getLong(String path) {
        return playerdata.getLong(path);
    }

    public String getString(String path) {
        return playerdata.getString(path);
    }

    public List<String> getStringList(String path) {
        return playerdata.getStringList(path);
    }

    public ConfigurationSection getConfigurationSection(String path) {
        return playerdata.getConfigurationSection(path);
    }

    public void removeData(String path) {
        playerdata.removeKey(path);
    }
}
